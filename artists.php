<?php include 'header.php';?>
  <!-- Start your project here-->
  <div class="section26">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 popupmodal">
          <h2 data-aos="fade-up">A Community<br>
Of the Artist, By the Artist,<br> For the Artist </h2>
          <p data-aos="fade-up" >Happydemic is a fast paced, music focused venture that enables artists like you to showcase your potential, improve your skills & connect with peers. We help you to succeed in doing what you were born to do.</p>
          <a data-aos="fade-up" href="#login" data-effect="mfp-zoom-in" class="btn btn-dark popme">Sign Up for free!</a> <a href="#watchourvideo" data-effect="mfp-zoom-in" class="btn btn-ligh popme" data-aos="fade-up"><i class="far fa-play-circle"></i> Watch our video</a>
        </div>
        <div class="col-sm-5">
          <img src="img/artist/banner.png" alt="" data-aos="zoom-out">
        </div>
      </div>
    </div>
  </div>
  <div class="section27">
    <div class="container">
      <div class="row desktop_none">
        <div class="col">
            <h2 data-aos="fade-up">Our Mission</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <h2 class="mobile_none"data-aos="fade-up">Our Mission</h2>
          <img src="img/artist/our_mission.png" alt=""  data-aos="zoom-out">
        </div>
        <div class="col-sm-6">
          <p data-aos="fade-up">Our mission is to successfully minimize the entry barriers for new emerging music talent & artists, to empower them in their journey towards becoming a music professional through constant nurturing and provide a platform for<br> sustainable
            growth. </p>
          <h5 data-aos="fade-up">Help us spread the word on</h5>
          <div class="social_sec" data-aos="fade-up">
            <ul >
              <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
              <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
              <li style="display:none"> <a href="#"><i class="fab fa-instagram"></i></a> </li>
              <li> <a href="#"><i class="fab fa-linkedin-in"></i></a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section28 container">
    <div class="row">
      <div class="col-sm-5">
        <h2 data-aos="fade-up">Here's what we offer</h2>
      </div>
      <div class="col-sm-7">
        <p data-aos="fade-up">Empowered with exponential growth opportunities, Happydemic enables artists to earn the recognition they truly deserve. We value your talent!</p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <div class="work_detail" data-aos="fade-up">
          <div class="icon">
          <img src="img/artist/1.svg" alt="">
          </div>
          <div class="content_detail">
            <h4>Flexible work terms</h4>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="work_detail" data-aos="fade-up">
          <div class="icon">
            <img src="img/artist/2.svg" alt="">
          </div>
          <div class="content_detail">
            <h4>Vibrant Work Culture </h4>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="work_detail" data-aos="fade-up">
          <div class="icon">
        <img src="img/artist/3.svg" alt="">
          </div>
          <div class="content_detail">
            <h4>Fast-paced Growth </h4>
          </div>
        </div>
      </div>
      <div class="col-sm-4 offset-sm-2">
        <div class="work_detail" data-aos="fade-up">
          <div class="icon">
          <img src="img/artist/4.svg" alt="">
          </div>
          <div class="content_detail">
            <h4>Respect for performance <br>& innovation</h4>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="work_detail" data-aos="fade-up">
          <div class="icon">
          <img src="img/artist/5.svg" alt="">
          </div>
          <div class="content_detail">
            <h4>Quality-driven Mentorship </h4>
          </div>
        </div>
      </div>

    </div>
  </div>
  <div class="section29">
    <div class="container">
      <div class="row">
        <div class="col">
          <h2 data-aos="fade-up">Musicians love Happydemic</h2>
          <div class="testimonial owl-carousel owl-theme" >
            <div class="item">
              <div class="left_detail">
                <p>Hi everybody, I am Prasanna, singer from Puducherry. I have been a part of Happydemic for the past 2 years. And what a beautiful journey it has been! I met a lot of new people, I made their day special. That is the main aim of an
                  artist - to meet new people, make them happy, make their day special. Happydemic gave this opportunity to me. They been very kind to me. They have helped me become a better musician. They helped me financially too. I wish to
                  continue working with Happydemic and continue to make people happy and make them feel special. Thank you Happydemic!</p>
              </div>
              <div class="right_detail">
                <div class="center_one">
                  <img src="img/artist/prasanna.png" alt="">
                  <div class="user_detail desktop_none">
                    <h4>Prasanna Adhisesha</h4>
                    <p>Gutarist, Mumbai</p>
                  </div>
                </div>

              </div>
              <div class="user_detail mobile_none">
                <h4>Prasanna Adhisesha</h4>
                <p>Gutarist, Mumbai</p>
              </div>
            </div>
            <div class="item">
              <div class="left_detail">
                <p>Hello everyone, I am Shubhashish. I am a singer and performer. I have been associated with Happydemic for about 2 years now. It is fun working with Happydemic. I have done SongStruck for them. It is great concept - performance wise
                  for me. I have learnt a lot from it as well. Thank you Happydemic. Also thank you for continuing with the performances online even in the lockdown. It was a pretty good idea. It was a really new, fun experience. They really know how
                  to keep their artists and their clients happy.</p>
              </div>
              <div class="right_detail">
                <div class="center_one">
                  <img src="img/artist/shubhasis.png" alt="">
                  <div class="user_detail desktop_none">
                    <h4>Shubhashish Shelgaonkar</h4>
                    <p>Gutarist, Mumbai</p>
                  </div>
                </div>
              </div>
              <div class="user_detail mobile_none">
                <h4>Shubhashish Shelgaonkar</h4>
                <p>Gutarist, Mumbai</p>
              </div>
            </div>
            <div class="item">
              <div class="left_detail">
                <p>Hey! Hi everyone, this is Varun Jain, a singer-songwriter and producer from Aligarh. So finally I have got this opportunity to share my experience working with Happydemic. It has been very great till now. I have done quite a few
                  gigs with Happydemic and I am still continuing to do them. Things run quite smoothly with Happydemic. They understand an artists' needs and they respect artists. They provide such a feel good environment for artists. I hope in the
                  future also things run this smoothly. Thank you Happydemic for everything you have given me till now. Thank you so much!</p>
              </div>
              <div class="right_detail">
                <div class="center_one">
                  <img src="img/artist/varun.png" alt="">
                  <div class="user_detail desktop_none">
                    <h4>Varun Jain</h4>
                    <p>Gutarist, Mumbai</p>
                  </div>
                </div>
              </div>
              <div class="user_detail mobile_none">
                <h4>Varun Jain</h4>
                <p>Gutarist, Mumbai</p>
              </div>
            </div>
            <div class="item">
              <div class="left_detail">
                <p>Hi, my name is Amrut Bhatt. I am drum circle facilitator from Mumbai. I am really happy that I have this opportunity of sharing my journey with Happydemic. First of all, I would like to thank the team for considering me to be on
                  board and for adding so much of value to my life as a professional musician and facilitator. Happydemic gives me an amazing opportunity to work with a variety of clients across the country. Each event is a beautiful experience! I
                  thank you guys for taking such good care of me throughout these years and ensuring that everything I require is taken care of and for being so nice to me the entire time. I thank each one of you for your effort and for making this
                  musical journey a possibility. Wishing all the best to team Happydemic and an amazing year ahead. Thank you so much guys. You are awesome!</p>
              </div>
              <div class="right_detail">
                <div class="center_one">
                  <img src="img/artist/amrut.png" alt="">
                  <div class="user_detail desktop_none">
                    <h4>Amrut Bhatt</h4>
                    <p>Gutarist, Mumbai</p>
                  </div>
                </div>

              </div>
              <div class="user_detail mobile_none">
                <h4>Amrut Bhatt</h4>
                <p>Gutarist, Mumbai</p>
              </div>
            </div>
            <div class="item">
              <div class="left_detail">
                <p>Hi guys, a merry merry Christmas to all of you! First of all, I would like to say Happydemic has been so crucial and so important in my life. They say you have to be grateful for the things in your life and I am the most grateful
                  to Happydemic for giving me so much of love, all throughout these 2 years. We have had a lot of projects together, worked a lot together, a lot of bonding together by now. All the team members are so humble and so helpful all the
                  time. I am so happy that I am associated with Happydemic and love you all from the team. Thank you so much. Merry Christmas again and a Happy 2021 to all of you. Thank you so much!</p>
              </div>
              <div class="right_detail">
                <div class="center_one">
                  <img src="img/artist/samir.png" alt="">
                  <div class="user_detail desktop_none">
                    <h4>Samiir S</h4>
                    <p>Gutarist, Mumbai</p>
                  </div>
                </div>
              </div>
              <div class="user_detail mobile_none">
                <h4>Samiir S</h4>
                <p>Gutarist, Mumbai</p>
              </div>
            </div>
            <div class="item">
              <div class="left_detail">
                <p>Hi everyone, this is Vikrant and I am from Himachal Pradesh. I have been working with Happydemic for about 2 years now. It has been an amazing experience. The best thing about Happydemic is that they promote their artists so well
                  and isn't that every artists' dream! Even in the lockdown they helped their artists with a steady inflow of money. Thank you so much Happydemic. I hope this partnership stays the same in the future as well. Thank you!</p>
              </div>
              <div class="right_detail">
                <div class="center_one">
                  <img src="img/artist/vikrant.png" alt="">
                  <div class="user_detail desktop_none">
                    <h4>Vikrant Rana</h4>
                    <p>Gutarist, Mumbai</p>
                  </div>
                </div>

              </div>
              <div class="user_detail mobile_none">
                <h4>Vikrant Rana</h4>
                <p>Gutarist, Mumbai</p>
              </div>
            </div>
            <div class="item">
              <div class="left_detail">
                <p>Not just 4 steps, my relationship with Happydemic is of 4 years. I have been associated with Happydemic since it was founded. My musical bond with Happydemic is a company that works with and for artists. They know really well how to organize a show keeping in mind the strengths of an artist. For this I, Budhaditya Mukherjee, am very thankful to Happydemic. They have trusted me and garnered trust in me that I can deliver a power packed performance. Happydemic bonds with us artists on a personal level and hence working with them is such fun. Thank you Happydemic for being with artists like me and hopefully this relationship will last this lifetime. Thank you!</p>
              </div>
              <div class="right_detail">
                <div class="center_one">
                  <img src="img/artist/budhitiya.png" alt="">
                  <div class="user_detail desktop_none">
                    <h4>Budhaditya Mukherjee</h4>
                    <p>Gutarist, Mumbai</p>
                  </div>
                </div>
              </div>
              <div class="user_detail mobile_none">
                <h4>Budhaditya Mukherjee</h4>
                <p>Gutarist, Mumbai</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section30 container">
    <div class="row">
      <div class="col">
        <div class="background_img">
          <h2 data-aos="fade-up">Our journey depicted in numbers </h2>
          <ul>
            <li data-aos="fade-up" >
              <h3>5000+</h3>
              <p>Artist Enrolled</p>
            </li>
            <li data-aos="fade-up"  data-aos-delay="900">
              <h3>40,000</h3>
              <p>Hour of musical <br>
                content created</p>
            </li>
            <li data-aos="fade-up"  data-aos-delay="1000">
              <h3>300+</h3>
              <p>Gigs performed</p>
            </li>
            <li data-aos="fade-up"  data-aos-delay="1100">
              <h3>500,000+</h3>
              <p>Streaming Fans</p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div id="watchourvideo" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/vwocKPxVmXY?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.testimonial').owlCarousel({
        stagePadding: 0,
        items: 1,
        loop: false,
        margin: 0,
        dots: true,
        autoplay: false,
        smartSpeed: 500,
        nav: false,
         autoHeight:true,
      });
      $('.nav__content').click(function() {
        $('.menu').removeClass('active');
        $('body').removeClass('nav-active');
      });
    });

  </script>
</body>

</html>
