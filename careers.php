<?php include 'header.php';?>
  <!-- Start your project here-->
  <div class="section44">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h2>If you subscribe to <span class="color-yellow">spreading</span><br>
<span class="color-yellow">Happiness,</span> join us.</h2>
          <p>The myriad-sided nature of the diversified musical talents has been a source of inspiration for Happydemic. This platform is for those who prefer authenticity with the pulse of artistic essence and appreciate outcomes and candid efforts.</p>
          <a href="#currentopening" class="btn btn-primary"> See current openings <img src="img/home/right_arrow.svg" alt=""> </a>
        </div>
        <div class="col-sm-6">
          <img src="img/careers/banner.png" alt="">
        </div>
      </div>
    </div>
  </div>
  <div class="section46">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <h2>Our Core values</h2>
        </div>
        <div class="col-sm-8">
          <div class="row">
            <div class="col-sm-6">
              <img src="img/careers/1.png" alt="">
              <h3>Collaboration</h3>
              <h4>Our competency to collaborate internally and externally through our unique initiatives is the key value that drives every Happydemician. Our initiatives include employee training and development, diversity, reward management and
                beyond. </h4>
            </div>
            <div class="col-sm-6">
              <img src="img/careers/2.png" alt="">
              <h3>Transperency</h3>
              <h4>We believe in maintaining complete transparency in dealing with our internal and external stakeholders encompassing our employees, artists, clients, and other key stakeholders. </h4>
            </div>
            <div class="col-sm-6">
              <img src="img/careers/3.png" alt="">
              <h3>Trust</h3>
              <h4>Happydemic puts a lot of value on Trust as an integral aspect for talents keen to shape their company’s career. Trust is indeed the aspect that helps us attract, develop and retain high-performing talent. </h4>
            </div>
            <div class="col-sm-6">
              <img src="img/careers/4.png" alt="">
              <h3>Integrity</h3>
              <h4>The commitment to keep talents secure is of paramount significance and is not compromised in any way. Since its inception, integrity has been at the fulcrum of our vision, aiming to connect musical talents with the talent scouts in
                the most seamless manner. </h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section47 container" id="currentopening">
    <div class="row">
      <div class="col">
        <h2>Let’s find you an open position </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <p>Email your resume at </p>
        <a href="mailto:careers@happydemic.com">careers@happydemic.com</a>
      </div>
      <div class="col-sm-8">
        <form class="" action="index.html" method="post">
          <div class="form-group">
            <select class="form-control">
              <option value="1">All Departments</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
              <option value="4">Four</option>
              <option value="5">Five</option>
              <option value="6">Six</option>
              <option value="7">Seven</option>
              <option value="8">Eight</option>
            </select>
            <div class="arrow_down"></div>
          </div>
          <button type="button" name="button" class="btn btn-primary">Find openings</button>
        </form>
        <h5 class="error">No Openings Currently</h5>
      </div>
    </div>
  </div>


  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.testimonials').owlCarousel({
        stagePadding: 0,
        items: 1,
        loop: false,
        margin: 30,
        dots: true,
        autoplay: false,
        smartSpeed: 1500,
        nav: false,
        navText: [
          "<i class='fas fa-chevron-left'></i>",
          "<i class='fas fa-chevron-right'></i>"
        ],
      });
  </script>
</body>

</html>
