<?php include 'header.php';?>
  <!-- Start your project here-->
  <div class="section38">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h4 data-aos="fade-up">SOLUTION FOR</h4>
          <h2 data-aos="fade-up">Brand Essence</h2>
        </div>
        <div class="col-sm-6">
          <p data-aos="fade-up">Helping your brand forge a strong emotional bond with your customers leads to improved business performance and deeper customer engagement. We help embed your brand with a strong musical engagement strategy combined with other branding efforts to create a consistent image in your clients' mind about who you are, what you do and what you stand for.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="section39_2" data-aos="zoom-up"></div>

  <div class="section40 container">
    <div class="row">
      <div class="col-sm-6 col-12">
        <h4 data-aos="fade-up">Brands matter.  And music for <br>
brand growth matters even more. </h4>
        <img data-aos="zoom-up"  src="img/casestudy/brand-essence/left.png" alt="" class="left_img">
      </div>
      <div class="col-sm-6 col-12 mobile_none">
        <img  data-aos="zoom-up" src="img/casestudy/brand-essence/right.png" alt="" class="right_img">
      </div>
    </div>

  </div>
  <div class="section40_2" data-aos="fade-up">
    <div class="container">
      <div class="row">
        <div class="col popupmodal">
          <a  href="#watchvidio" class="popme"data-effect="mfp-zoom-in">  <img src="img/casestudy/playbutton.svg" alt=""></a>
        </div>
      </div>
    </div>
  </div>

  <div class="section41" id="brand-essence">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="social_sec mobile_none" data-aos="fade-up">
            <ul>
              <li>Share</li>
              <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
              <li> <a href="#"><i class="fab fa-instagram"></i></a> </li>
              <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
              <li> <a href="#"><i class="fab fa-linkedin-in"></i></a> </li>
            </ul>
          </div>
          <div class="content_center">
            <h2 data-aos="fade-up">Case Studies</h2>
            <div class="brand_logo" data-aos="fade-up">
              <img src="img/casestudy/brand-essence/acc.png" alt="">
              <h3 >ACC Case study</h3>
              <h4>Industry : Cement & Infrastructure</h4>
            </div>
            <div class="content">
              <h4 data-aos="fade-up">Context/ Problem</h4>
              <p data-aos="fade-up">ACC Cements Limited was launching a new product ACC Gold and wanted to do so in an innovative manner</p>
              <h4 data-aos="fade-up">Solution</h4>
          <p data-aos="fade-up">Happydemic team proposed to use SongStruck to reach out select dealers and introduce the product with a musical experience. </p>
              <h4 data-aos="fade-up">Impact</h4>
                <p data-aos="fade-up">i) Dealers enjoyed music as a medium of communication. In Ranchi 100% of the experiences received Very good & Excellent Ratings. In Patna 93% of the experiences received Very good & Excellent Ratings.<br>
                ii) The dealers saw the novelty value in such experiences and were highly engaged leaving many of them wanting more.<br>
                iii) One of the dealers placed an order of 210 ACC Gold bags instantly.<br>
                iv) This helped ACC stand out in their activation leading to higher retention and recall value for the brand. <br>
                v) Music helped emotionally connect with the POCs in turn bettering relationships.
                </p>
            <!-- <iframe data-aos="fade-up" width="100%" height="413px" src="https://www.youtube.com/embed/pKwNDsBxXRA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
            <div class="social_sec desktop_none" data-aos="fade-up">
              <ul>
                <li>Share</li>
                <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
                <li> <a href="#"><i class="fab fa-instagram"></i></a> </li>
                <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
                <li> <a href="#"><i class="fab fa-linkedin-in"></i></a> </li>
              </ul>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- <div class="section42">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 data-aos="fade-up">Happymonials</h2>
          <div class="testimonial_client owl-carousel owl-theme">
            <div class="item">
              <div class="content_sec">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
              <h4>Palash Nandy</h4>
              <h5>CEO, Numeric UPS</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> -->

  <div class="section43" data-aos="fade-up">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ul>
            <li> <a href="corporate-offerings.php">Back :<br> Corporate Offerings</a> </li>
            <li> <a href="revenue-impact.php">Next :<br> Revenue Impact</a> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="section10">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="gry_text" data-aos="fade-up">GET IN TOUCH</div>
          <h2 data-aos="fade-up">Let’s connect</h2>
        </div>
        <div class="col-sm-6">
          <form action="/action_page.php">
            <div class="form-group" data-aos="fade-up">
              <label for="usr">NAME</label>
              <input type="text" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">CONTACT NUMBER</label>
              <input type="tel" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">EMAIL ID</label>
              <input type="email" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">NATURE OF ENQUIRY</label>
              <select class="form-control" name="">
                  <option value="">Corporate enquiries </option>
                  <option value="">Songstruck enquiries </option>
                  <option value="">Artist enquiries </option>
                  <option value="">Other enquiries </option>
              </select>
                <div class="arrow_down"></div>
            </div>
            <button data-aos="fade-up" type="button" class="btn btn-dark">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div id="watchvidio" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/LW42rvbo89A?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.testimonial_client').owlCarousel({
        stagePadding: 0,
        items: 2,
        loop: false,
        margin: 30,
        dots: true,
        autoplay: false,
        smartSpeed: 500,
        nav: false,
        center:true,
        responsive : {
            0 : {
            items: 1,
            nav: false,
            margin: 10
            },
            768 : {
            items: 2,
            }
        }
      });
  </script>
</body>

</html>
