
<?php include 'header.php';?>
<!-- Start your project here-->
<div class="section53">
  <div class="container">
    <div class="row">
      <div class="col">
        <h2 data-aos="fade-up">The thrill of a concert,<br> right at your home!</h2>
        <p data-aos="fade-up">Songstruck features a diverse lineup of artists who will entertain you from the comfort of your home and make every moment or occasion special. </p>
        <div class="btn_click">
          <a data-aos="fade-up" href="#bookesongstruck" class="btn btn-warning">Book e-SongStruck</a>
          <a data-aos="fade-up" href="#" class="btn btn-warning btn-trans"><i class="far fa-play-circle"></i> Watch our video</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="section54">
  <div class="container">
    <div class="row">
      <div class="col">
        <h2 data-aos="fade-up">Own your SongStruck in just 3 steps!</h2>
      </div>
    </div>
    <div class="row invert">
      <div class="col-sm-6">
        <div class="step_counter owl-carousel owl-theme">
          <div class="item" data-dot="<button>1</button>">
            <img src="img/songstruck/step.png" alt="">
          </div>
          <div class="item" data-dot="<button>2</button>">
            <img src="img/songstruck/step2.png" alt="">
          </div>
          <div class="item" data-dot="<button>3</button>">
            <img src="img/songstruck/step3.png" alt="">
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <ul class="slider_step" data-aos="fade-up">
          <li>
            <div class="number desktop_none">
              1
            </div>
            <h3>Brief us about our request</h3>
            <h4>Name the songs or genres of your request - any language, Bollywood or International!!</h4>
          </li>
          <li>
            <div class="number desktop_none">
              2
            </div>
            <h3>Confirm and make payment</h3>
            <h4>Once you fill your details in the form given below, go right ahead and make your payment. </h4>
          </li>
          <li>
            <div class="number desktop_none">
              3
            </div>
            <h3>Enjoy along with your loved ones</h3>
            <h4>Sit back and relax. You will have your concert served right at your doorstep (or on your Zoom window) in case you prefer e-Songstruck. </h4>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="section55">
  <div class="container">
    <div class="row">
      <div class="col">
        <h2 data-aos="fade-up">Book an e-SongStruck for every moment</h2>
        <ul data-aos="fade-up">
          <li>Birthdays</li>
          <li>Anniversaries</li>
          <li>Congratulations</li>
          <li>Get well soon</li>
          <li>Miss you</li>
          <li>Proposal</li>
        </ul>
      </div>
    </div>

    <div class="row" >
      <div class="col-sm-6">
        <img data-aos="fade-up" src="img/songstruck/col1.png" alt="">
      </div>
      <div class="col-sm-6 mobile_none">
        <div class="row last_col">
          <div class="col-sm-6">
              <img src="img/songstruck/col2.png" alt="" data-aos="fade-up">
          </div>
          <div class="col-sm-6">
            <img src="img/songstruck/col3.png" alt="" data-aos="fade-up">
          </div>
          <div class="col-sm-6">
              <img src="img/songstruck/col4.png" alt="" data-aos="fade-up">
          </div>
          <div class="col-sm-6 last_img">
            <img src="img/songstruck/col5.png" alt="" data-aos="fade-up">
          </div>
        </div>
      </div>
      <div class="col-sm-6 desktop_none">
          <img src="img/songstruck/mobile.png" alt="" data-aos="fade-up">
      </div>
    </div>
  </div>
</div>
<div class="section56">
  <div class="container">
    <div class="row">
      <div class="col">
        <h2 data-aos="fade-up"> <span>What our patrons have <br>to say about eSongstruck</span> </h2>
        <div class="esongstruck owl-carousel owl-theme">
          <div class="item">
            <p>A fantastic initiative to make it a memorable time with family at home. Enjoyed with my parents and family</p>
            <h3>Sandeep Sharma</h3>
            <h4>Mumbai, India</h4>
          </div>
          <div class="item">
            <p>The esongstruck is an amazing concept especially since it has a personal touch. The experience was truely magical for me and my family. The customisation with choice of genre of songs makes it exeptional. Rhishav is a great artist and
              though it is impossible to match great yesteryear's artists, he made it extra special with his talent. Kudos to him.
              Thank you so much for this experience!!
            </p>
            <h3>Shibani Deshmukh</h3>
            <h4>Mumbai, India</h4>
          </div>
          <div class="item">
            <p>Till now we only use to arrange for customers but experiencing the same is really different experience. A day full of work, meetings, followed by a soothing and relaxing evening through e-SongStruck...Thanks Team Happydemic. It was
              awesome and struck the musical nostalgia</p>
            <h3>Sumit Khedekar</h3>
            <h4>Mumbai, India</h4>
          </div>
          <div class="item">
            <p>It was a great experience to have live artist on digital platform for birthday celebration. Something different than ordinary celebrations. It has made my day special. Thanks Happydemic Team.</p>
            <h3>Jayesh Makwana</h3>
            <h4>Mumbai, India</h4>
          </div>
          <div class="item">
            <p>It was a beautiful performance. Changed the energy in our house.</p>
            <h3>Lloyd Noronha</h3>
            <h4>Mumbai, India</h4>
          </div>
          <div class="item">
            <p>This was adorable... Thanks so much absolutely enjoyed.</p>
            <h3>Gargee Deshmukh</h3>
            <h4>Mumbai, India</h4>
          </div>
          <div class="item">
            <p>It was great! My mom experienced something like this for the first time. Thanks team Happydemic!</p>
            <h3>Shruti Hari</h3>
            <h4>Mumbai, India</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="section57" id="bookesongstruck">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <h2 data-aos="fade-up">You take the first step,<br>
          we’ll take it from there!</h2>
        <div data-aos="fade-up" class="image_sec mobile_none">
          <img src="img/songstruck/form_img.png" alt="">
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form_sec">
          <h3 data-aos="fade-up">Share your requirements</h3>
          <p data-aos="fade-up">Our representative will get in touch with you soon.</p>
          <div class="form_devid">
            <form action="/action_page.php">
              <div class="form-group" data-aos="fade-up">
                <label for="usr">Your Full Name</label>
                <input type="text" class="form-control">
              </div>
              <div class="form-group" data-aos="fade-up">
                <label for="email">Email</label>
                <input type="email" class="form-control">
              </div>
              <div class="form-group" data-aos="fade-up">
                <label for="tel">WhatsApp Contact Number</label>
                <input type="tel" class="form-control">
              </div>
              <div class="form-group" data-aos="fade-up">
                <label for="date">Date of performance</label>
                <input type="date" class="form-control">
              </div>
                <div class="clear_both"></div>
              <div class="form-group text_area" data-aos="fade-up">
                <label for="text">Your song/message </label>
                <textarea name="text" rows="8" ></textarea>
              </div>
              <p data-aos="fade-up">By continuing you agree to our <a href="#">Terms and Conditions</a> </p>
              <button data-aos="fade-up" type="button" class="btn btn-dark">Book now at ₹2500 + Taxes</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="section58">
  <div data-aos="fade-up" class="image_sec desktop_none">
    <img src="img/songstruck/form_img.png" alt="">
  </div>
  <div class="container">
    <div class="row">
      <div class="col">

        <h3 data-aos="fade-up">Frequently Asked Questions</h3>
        <div class="accordion accordion-flush" id="accordion" data-aos="fade-up">
        <div class="left_sec">
          <div class="accordion-item">
            <h2 class="accordion-header" id="one">
              <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#one_sec" aria-expanded="false" aria-controls="one_sec">
                What is e-SongStruck?
              </button>
            </h2>
            <div id="one_sec" class="accordion-collapse collapse" aria-labelledby="one" data-mdb-parent="#accordion">
              <div class="accordion-body">
              A 15-minute digital musical performance dedicated to your dear ones. A hyper-personalised experience shared by you, the recipient, and your immediate family members.
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="two">
              <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#two_sec" aria-expanded="false" aria-controls="two_sec">
                How many people can I have on the call to enjoy the performance?
              </button>
            </h2>
            <div id="two_sec" class="accordion-collapse collapse" aria-labelledby="two" data-mdb-parent="#accordion">
              <div class="accordion-body">
              e-SongStruck is a digital concert to be enjoyed by your family members. We would request you to keep the number of people to 6-7.
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="three">
              <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#three_sec" aria-expanded="false" aria-controls="three_sec">
              How to place a request?
              </button>
            </h2>
            <div id="three_sec" class="accordion-collapse collapse" aria-labelledby="three" data-mdb-parent="#accordion">
              <div class="accordion-body">
            Fill in the form with all the details and make the payment. A Happydemic team member will get in touch with you to coordinate your request. Alternatively, you can contact us at e-songstruck@happydemic.com
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="four">
              <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#four_sec" aria-expanded="false" aria-controls="four_sec">
            How much will I be charged if I want an hour’s performance or more than one artist?
              </button>
            </h2>
            <div id="four_sec" class="accordion-collapse collapse" aria-labelledby="four" data-mdb-parent="#accordion">
              <div class="accordion-body">
          Yes, we do cater to performances other than e-SongStruck as well. For more details contact us at e-songstruck@happydemic.com
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="five">
              <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#five_sec" aria-expanded="false" aria-controls="five_sec">
        Can I personalize the performance?
              </button>
            </h2>
            <div id="five_sec" class="accordion-collapse collapse" aria-labelledby="five" data-mdb-parent="#accordion">
              <div class="accordion-body">
        Upon receiving your booking, a Happydemic team member will get in touch with you and help curate a 15-minute mashup of Bollywood songs incorporating 2-3 songs that hold importance to the recipient and a few popular songs that will make the performance memorable.
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="seven">
              <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#seven_sec" aria-expanded="false" aria-controls="seven_sec">
      What platform will this performance be delivered on?
              </button>
            </h2>
            <div id="seven_sec" class="accordion-collapse collapse" aria-labelledby="seven" data-mdb-parent="#accordion">
              <div class="accordion-body">
      e-SongStruck is typically delivered through a Whatsapp video call or a Zoom call.
              </div>
            </div>
          </div>
          <div class="accordion-item">
            <h2 class="accordion-header" id="eight">
              <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#eight_sec" aria-expanded="false" aria-controls="eight_sec">
        If it is a Zoom call, can we add more friends/family members for the performance?
              </button>
            </h2>
            <div id="eight_sec" class="accordion-collapse collapse" aria-labelledby="eight" data-mdb-parent="#accordion">
              <div class="accordion-body">
        As e-SongStruck is a hyper-personalised performance, it is ideally for the recipient and their family members. We would request you to keep the number of people joining in to 6-7 people.
              </div>
            </div>
          </div>
        </div>
<div class="right_sec">
  <div class="accordion-item">
    <h2 class="accordion-header" id="nine">
      <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#nine_sec" aria-expanded="false" aria-controls="nine_sec">
There is about 12 hr difference in my time and yours. How do you plan to manage?
      </button>
    </h2>
    <div id="nine_sec" class="accordion-collapse collapse" aria-labelledby="nine" data-mdb-parent="#accordion">
      <div class="accordion-body">
For us to deliver the best experience to you, we would request a window of 48 hours between the booking and delivery of your request.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="ten">
      <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#ten_sec" aria-expanded="false" aria-controls="ten_sec">
Will the artist be from India?
      </button>
    </h2>
    <div id="ten_sec" class="accordion-collapse collapse" aria-labelledby="ten" data-mdb-parent="#accordion">
      <div class="accordion-body">
Happydemic has an array of artists from across India on board with us. As per your requirement, we will choose the artist that best suits your occasion
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="eleven">
      <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#eleven_sec" aria-expanded="false" aria-controls="eleven_sec">
What languages can the artists sing in?
      </button>
    </h2>
    <div id="eleven_sec" class="accordion-collapse collapse" aria-labelledby="eleven" data-mdb-parent="#accordion">
      <div class="accordion-body">
Typically e-SongStruck is delivered in Bollywood or English songs. However, if you have a specific request of other Indian languages, we will try to accommodate the same
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="twel">
      <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#twel_sec" aria-expanded="false" aria-controls="twel_sec">
How will I know which artist will perform for me?
      </button>
    </h2>
    <div id="twel_sec" class="accordion-collapse collapse" aria-labelledby="twel" data-mdb-parent="#accordion">
      <div class="accordion-body">
All our artists are professional singers and are adept at delivering the experience and making the day/occasion special for you and your loved ones. Happydemic will pick an artist best suited to your requirement.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="thirteen">
      <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#thirteen_sec" aria-expanded="false" aria-controls="thirteen_sec">
Does the artist perform based on requests or his/her own selection of songs?
      </button>
    </h2>
    <div id="thirteen_sec" class="accordion-collapse collapse" aria-labelledby="thirteen" data-mdb-parent="#accordion">
      <div class="accordion-body">
The artist prepares a list of songs basis the details shared by you. They will try and cater to requests received on the spot to the best of their capacity as well
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="forteen">
      <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#forteen_sec" aria-expanded="false" aria-controls="forteen_sec">
Do you only have e-SongStruck or do you offer other products too?
      </button>
    </h2>
    <div id="forteen_sec" class="accordion-collapse collapse" aria-labelledby="forteen" data-mdb-parent="#accordion">
      <div class="accordion-body">
We offer a basket of concepts. For more details contact us at e-songstruck@happydemic.com
      </div>
    </div>
  </div>
</div>


        </div>
      </div>
    </div>
  </div>
</div>
<?php include 'footer.php';?>
<!-- <script src="js/masonry.js"></script> -->

<!-- Custom scripts -->
<script type="text/javascript">
  $(document).ready(function() {
    $('.step_counter').owlCarousel({
      stagePadding: 0,
      items: 1,
      loop: true,
      autoplay: true,
      autoplayTimeout: 2000,
      autoplaySpeed: 2000,
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      margin: 0,
      singleItem: true,
      nav: false,
      dotsData: true,
      responsive : {
          0 : {
          items: 1,
          nav: false,
          margin: 10,
          dots:false,
          },
          768 : {
          items: 1,
          }
      }
    });
    $('.esongstruck').owlCarousel({
      items: 2,
      loop: false,
      margin: 100,
      nav: true,
      dots:false,
      nav: true,
      autoHeight:true,
      navText: [
        "<img src='img/songstruck/left_arow.svg'>",
        "<img src='img/songstruck/right_arow.svg'>"
      ],
      responsive : {
          0 : {
          items: 1,
          margin: 10,
          dots:false,
          },
          768 : {
          items: 1,
          }
      }
    });
    });

</script>
</body>

</html>
