<?php include 'header.php';?>
  <!-- Start your project here-->
  <div class="section38">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h4 data-aos="fade-up">SOLUTION FOR</h4>
          <h2 data-aos="fade-up">Revenue Impact</h2>
        </div>
        <div class="col-sm-6">
<p data-aos="fade-up">Music-inspired activities involve multitude parts of your brain encompassing emotional, cognitive and motor areas. Promote intuitive bonding mechanisms amongst your employees to boost revenue impact through our remarkable musical engagement solutions. </p>
      </div>
      </div>
    </div>
  </div>
  <div class="section39_3"></div>
  <div class="section40 container">
    <div class="row">
      <div class="col-sm-6 col-12">
        <h4 data-aos="fade-up">Musical engagement with <br>
channel partners for results</h4>
        <img data-aos="fade-up" src="img/casestudy/revenue-impact/left.png" alt="" class="left_img">
      </div>
      <div class="col-sm-6 col-12 mobile_none">
        <img data-aos="fade-up" src="img/casestudy/revenue-impact/right.png" alt="" class="right_img">
      </div>
    </div>

  </div>
  <div class="section40_2 " data-aos="fade-up">
    <div class="container">
      <div class="row">
        <div class="col popupmodal">
          <a  href="#watchvidio" data-effect="mfp-zoom-in" class="popme">  <img src="img/casestudy/playbutton.svg" alt=""></a>
        </div>
      </div>
    </div>

  </div>
  <div class="section41" id="revenue-impact">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="social_sec mobile_none" data-aos="fade-up">
            <ul>
              <li>Share</li>
              <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
              <li> <a href="#"><i class="fab fa-instagram"></i></a> </li>
              <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
              <li> <a href="#"><i class="fab fa-linkedin-in"></i></a> </li>
            </ul>
          </div>
          <div class="content_center">
            <h2 data-aos="fade-up">Case Studies</h2>
            <div class="brand_logo" data-aos="fade-up">
              <img data-aos="fade-up" src="img/casestudy/revenue-impact/axis.png" alt="">
              <h3 data-aos="fade-up">AXIS Mutual Fund </h3>
              <h4 data-aos="fade-up">Industry : Financial Services </h4>
            </div>
            <div class="content">
              <h4 data-aos="fade-up">Context/ Problem</h4>
<p>The need of the hour was to engage with IFAs in an innovative way to gain an extra edge over competition.
</p>
              <h4 data-aos="fade-up">Solution</h4>
        <p>Engaging beyond business and making IFAs do something they have never done before was where the answer was. With the aim of bringing everyone together with something unconventional gave birth to Axis MF Idol – The singing talent hunt.     </p>
              <h4 data-aos="fade-up">Impact</h4>
              <p data-aos="fade-up">The response received was overwhelming and this initiative played a vital role in building an IFA community like never before.  </p>
            <iframe width="100%" height="413px" src="https://www.youtube.com/embed/fIv4wbahtBw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="social_sec desktop_none" data-aos="fade-up">
              <ul>
                <li>Share</li>
                <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
                <li> <a href="#"><i class="fab fa-instagram"></i></a> </li>
                <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
                <li> <a href="#"><i class="fab fa-linkedin-in"></i></a> </li>
              </ul>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section42">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 data-aos="fade-up">Happymonials</h2>
          <div class="testimonial_client owl-carousel owl-theme">
            <div class="item">
              <div class="content_sec">
                <!-- <h3>Incredible experience</h3> -->
              <p>The Happydemic team mounted a very unique engagement property for us in MF Idol. The program gave us an excellent opportunity to engage with our intermediaries beyond business. The team curated and executed the program quite well. We had drawn a detailed plan with the Happydemic team at the beginning of the engagement and things just happened as per the envisaged plan. There were multiple legs to the engagement activity, right from managing the portal and social media group, holding semi finals and finals, putting together the jury and getting them actively involved in each step of the screening process, interfacing with the intermediaries etc etc. All these elements were seamlessly joined and impeccably executed by the Happydemic team. I would vouch for their capabilities across the spectrum of ideation as well as execution.</p>
              </div>
              <h4>Rohan Padhye</h4>
              <h5>VP Marcom & Digital Marketing</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section43" data-aos="fade-up">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ul>
            <li> <a href="brand-essence.php">Back :<br> Brand Essence</a> </li>
            <li> <a href="organisation-culture.php">Next :<br> Organisation & Culture</a> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="section10">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="gry_text" data-aos="fade-up">GET IN TOUCH</div>
          <h2 data-aos="fade-up">Let’s connect</h2>
        </div>
        <div class="col-sm-6">
          <form action="/action_page.php">
            <div class="form-group" data-aos="fade-up">
              <label for="usr">NAME</label>
              <input type="text" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">CONTACT NUMBER</label>
              <input type="tel" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">EMAIL ID</label>
              <input type="email" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">NATURE OF ENQUIRY</label>
              <select class="form-control" name="">
                  <option value="">Corporate enquiries </option>
                  <option value="">Songstruck enquiries </option>
                  <option value="">Artist enquiries </option>
                  <option value="">Other enquiries </option>
              </select>
                <div class="arrow_down"></div>
            </div>
            <button data-aos="fade-up" type="button" class="btn btn-dark">Submit</button>
          </form>
        </div>

      </div>
    </div>
  </div>
  <div id="watchvidio" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/StXyZF9-r-o?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.testimonial_client').owlCarousel({
        stagePadding: 0,
        items: 2,
        loop: false,
        margin: 30,
        dots: true,
        autoplay: false,
        smartSpeed: 500,
        nav: false,
        center:true,
        autoHeight:true,
        responsive : {
            0 : {
            items: 1,
            nav: false,
            margin: 10
            },
            768 : {
            items: 2,
            }
        }
      });
      });

  </script>
</body>

</html>
