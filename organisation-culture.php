<?php include 'header.php';?>
  <!-- Start your project here-->
  <div class="section38">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h4 data-aos="fade-up">SOLUTION FOR</h4>
          <h2 data-aos="fade-up">Organisation and Culture</h2>
        </div>
        <div class="col-sm-6">
          <p data-aos="fade-up">If your belief resonates with our belief that human assets are not meant to be treated like robots, let our music-infused innovations in engagement transform your organisational culture oozing with enthusiasm and positivity.  </p>
        </div>
      </div>
    </div>
  </div>
  <div class="section39"></div>
  <div class="section40 container">
    <div class="row">
      <div class="col-sm-6 col-12">
        <h4 data-aos="fade-up">Problems we solve to<br>
          enhance organisation culture</h4>
        <img data-aos="fade-up" src="img/casestudy/left.png" alt="" class="left_img">
      </div>
      <div class="col-sm-6 col-12 mobile_none">
        <img data-aos="fade-up" src="img/casestudy/right.png" alt="" class="right_img">
      </div>
    </div>

  </div>
  <div class="section40_2 " data-aos="fade-up">
    <div class="container">
      <div class="row">
        <div class="col popupmodal">
          <a  href="#watchvidio" data-effect="mfp-zoom-in" class="popme">  <img src="img/casestudy/playbutton.svg" alt=""></a>
        </div>
      </div>
    </div>

  </div>
  <div class="section41" id="organisation-culture">
    <div class="container">
      <div class="row">
        <div class="col-12 paddown">
          <div class="social_sec mobile_none" data-aos="fade-up">
            <ul>
              <li>Share</li>
              <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
              <li> <a href="#"><i class="fab fa-instagram"></i></a> </li>
              <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
              <li> <a href="#"><i class="fab fa-linkedin-in"></i></a> </li>
            </ul>
          </div>
          <!-- Pills navs -->
          <ul class="nav nav-pills mb-3" id="center_tab" role="tablist" data-aos="fade-up">
            <li class="nav-item" role="presentation">
              <div class="others active" id="numeric" data-mdb-toggle="pill" href="#jio_center" role="tab" aria-controls="jio_center" aria-selected="true" >
                <h2>Other Case Studies</h2>
                <div class="others_content">
                    <img src="img/casestudy/jio.png" alt="">
                    <div class="text_sec">
                      <h3>Reliance Jio</h3>
                      <p>Telecommunication Industry</p>
                    </div>
                </div>
              </div>
            </li>
            <li class="nav-item" role="presentation">
              <div class="others " id="jio" data-mdb-toggle="pill" href="#numeric_center" role="tab" aria-controls="numeric_center" aria-selected="false">
                 <h2>Other Case Studies</h2>
                 <div class="others_content">
                     <img src="img/casestudy/numeric.png" alt="">
                     <div class="text_sec">
                       <h3>Numeric UPS</h3>
                       <p>Manufacturing Industry</p>
                     </div>
                 </div>
               </div>
            </li>
          </ul>
          <!-- Pills navs -->

          <!-- Pills content -->
          <div class="tab-content" id="ex1-content" >
            <div  class="tab-pane fade show active" id="jio_center" role="tabpanel" aria-labelledby="jio" >
              <div class="content_center">
                <h2 data-aos="fade-up">Case Studies</h2>
                <div class="brand_logo">
                  <img data-aos="fade-up" src="img/casestudy/jio.png" alt="">
                  <h3 data-aos="fade-up">Reliance JIO</h3>
                  <h4 data-aos="fade-up">Industry : Telecommunication</h4>
                </div>
                <div class="content">
                  <h4 data-aos="fade-up">Context/ Problem</h4>
                  <p data-aos="fade-up">Reliance Jio, with the team spread across India, wished to engage every employee across hierarchy and geography. Through this engagement they looked to create a “One Company” feel.</p>
                  <h4 data-aos="fade-up">Solution</h4>
                  <p data-aos="fade-up">Happydemic identified the objective to engage beyond the professional aspect and nurture the employees’ hidden talent. We facilitated a singing talent hunt configured in two rounds – an online audition and a Grand Finale. <br>
                    Reliance “Jio Dil Se”, the singing talent hunt was a 6 week long engagement covering over one lakh members. The finalists were mentored by the jury on various parameters that make an artist a performer. They then performed to an
                    audience
                    comprising their colleagues and senior management.
                  </p>
                  <h4 data-aos="fade-up">Impact</h4>
                  <p data-aos="fade-up">The community created for this program is now a hotspot for the organisation’s other ongoing activities and everyone connects with it, dil se dil tak</p>
                <iframe data-aos="fade-up" width="100%" height="413px" src="https://www.youtube.com/embed/wSt5iq_y2Ds" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="numeric_center" role="tabpanel" aria-labelledby="numeric">
              <div class="content_center">
                <h2 data-aos="fade-up">Case Studies</h2>
                <div class="brand_logo">
                  <img data-aos="fade-up" src="img/casestudy/numeric.png" alt="">
                  <h3 data-aos="fade-up">Numeric UPS</h3>
                  <h4 data-aos="fade-up">Industry : Electrical Equipments </h4>
                </div>
                <div class="content">
                  <h4 data-aos="fade-up">Context/ Problem</h4>
                  <p data-aos="fade-up">Management of Numeric UPS wished to bring out a radical shift in the mindset of team members - from being employees to being stakeholders. They also wanted to motivate people to raise the bar, think new, act new, and infuse fresh energy to take decision through their tag line - New Energy to Power</p>
                  <h4 data-aos="fade-up">Solution</h4>
                <p data-aos="fade-up">Happydemic curated an original anthem for Numeric UPS that not only helped to launch their new tagline “New Energy to Power” but also helped them to communicate the group’s ambition 2020 tagline – “One Team. One Goal. One Pride”</p>
                  <h4 data-aos="fade-up">Impact</h4>
                  <p data-aos="fade-up">The song helped to:<br>
                    • bring new lease of freshness in thought and action<br>
                    • infuse new energy, the energy that will translate into shift in attitude and approach
                  </p>
                <iframe data-aos="fade-up" width="100%" height="413px" src="https://www.youtube.com/embed/Hw0a7iPRc4w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
              </div>
            </div>
          </div>
          <div class="social_sec desktop_none" data-aos="fade-up">
            <ul>
              <li>Share</li>
              <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
              <li> <a href="#"><i class="fab fa-instagram"></i></a> </li>
              <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
              <li> <a href="#"><i class="fab fa-linkedin-in"></i></a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section42 jio_test">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 data-aos="fade-up">Happymonials</h2>
          <div class="testimonial_client owl-carousel owl-theme">
            <div class="item">
              <div class="content_sec">
                <p>It was a great event and evening, I really enjoyed myself and it was probably the best event I’ve attended in my professional career of 20 years.</p>
              </div>
              <h4>Satyadeep Mishra</h4>
              <h5>Head of HR & Digital Services
              </h5>
            </div>
            <div class="item">
              <div class="content_sec">
                <!-- <h3>I really enjoyed myself</h3> -->
                <p>The entire concept of Jio Dil Se was so beautiful, it was a lovely way to engage all the employees. Working with the entire Happydemic team on this event was really fun, they produced some amazing creatives and were very patient in handling last minute requests. </p>
              </div>
              <h4>Prajakta Malgi</h4>
              <h5>Head - Internal Communications</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section42 num_test">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 data-aos="fade-up">Happymonials</h2>
          <div class="testimonial_client2 owl-carousel owl-theme">
            <div class="item">
              <div class="content_sec">
              <p data-aos="fade-up">The Numeric anthem that you guys created for us is becoming an internal war cry! Thanks again for the same...to your entire team.</p>
               </div>
              <h4 data-aos="fade-up">Palash Nandy</h4>
              <h5 data-aos="fade-up">CEO, Numeric UPS</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section43" data-aos="fade-up">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <ul>
            <li> <a href="revenue-impact.php">Back :<br> Revenue Impact</a> </li>
            <li> <a href="corporate-offerings.php">Next :<br> Corporate Offerings</a> </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="section10">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="gry_text" data-aos="fade-up">GET IN TOUCH</div>
          <h2 data-aos="fade-up">Let’s connect</h2>
        </div>
        <div class="col-sm-6">
          <form action="/action_page.php">
            <div class="form-group" data-aos="fade-up">
              <label for="usr">NAME</label>
              <input type="text" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">CONTACT NUMBER</label>
              <input type="tel" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">EMAIL ID</label>
              <input type="email" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">NATURE OF ENQUIRY</label>
              <select class="form-control" name="">
                  <option value="">Corporate enquiries </option>
                  <option value="">Songstruck enquiries </option>
                  <option value="">Artist enquiries </option>
                  <option value="">Other enquiries </option>
              </select>
                <div class="arrow_down"></div>
            </div>
            <button data-aos="fade-up" type="button" class="btn btn-dark">Submit</button>
          </form>
        </div>

      </div>
    </div>
  </div>
  <div id="watchvidio" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/DJD10nJTee0?list=PLPcX13Bwg_9blhkfv8ugoMOA_ibjg_dtF?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.num_test').hide();
      $('#jio').click(function() {
        $('.jio_test').hide();
        $('.num_test').show();
      });
      $('#numeric').click(function() {
        $('.jio_test').show();
        $('.num_test').hide();
      });

      $('.testimonial_client').owlCarousel({
        stagePadding: 0,
        items: 2,
        loop: false,
        margin: 30,
        dots: true,
        autoplay: false,
        smartSpeed: 500,
        nav: false,
        autoHeight:true,
        responsive : {
            0 : {
            items: 1,
            nav: false,
            margin: 10
            },
            768 : {
            items: 2,
            }
        }
      });
      $('.testimonial_client2').owlCarousel({
        stagePadding: 0,
        items: 2,
        center:true,
        loop: false,
        margin: 30,
        dots: true,
        autoplay: false,
        smartSpeed: 500,
        nav: false,
          autoHeight:true,
        responsive : {
            0 : {
            items: 1,
            nav: false,
            margin: 10
            },
            768 : {
            items: 2,
            }
        }
      });
      });

  </script>
</body>

</html>
