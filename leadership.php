<?php include 'header.php';?>
  <!-- Start your project here-->
  <div class="section33">
    <div class="container">
      <div class="row">
        <div class="col-sm-5">
          <h2 data-aos="fade-up">What makes <span class="color-yellow">us</span>, us?</h2>
          <p data-aos="fade-up">We are different individuals from varied backgrounds coming together for a common passion - music.
We love wrapping our heads around challenges and think creatively every day. </p>
        </div>
        <div class="col-sm-7 mobile_none">
          <img src="img/leadership/banner.png" alt="" data-aos="zoom-up">
        </div>
        <div class="col-sm-7 desktop_none">
          <img src="img/leadership/banner_mobile.png" alt="" data-aos="zoom-up">
        </div>
      </div>
    </div>
  </div>
  <div class="section33_2">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h3 data-aos="fade-up">Our Leadership</h3>
        </div>
      </div>
    </div>
  </div>
  <div class="section34 ">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-12">
          <img src="img/leadership/radhika mukherji.png" alt="" data-aos="fade-up">
        </div>
        <div class="col-sm-6 col-12">
          <h3 data-aos="fade-up">Radhika Mukherji</h3>
          <h4 data-aos="fade-up">Co-Founder & CEO</h4>
          <p data-aos="fade-up">Radhika Mukherji is a thriving entrepreneur with creative instincts and passion for Music. She uses her unique sense of technique and approach to resolve issues. Her primary motto is to enhance the joy of music enriching one’s soul. Being married to Shaan, she understands music, it's impact and the pain points that aspiring artists face.
            From creating musical IPs to flying, she firmly believes, she is the <strong>C</strong>hief <strong>E</strong>verything <strong>O</strong>fficer</p>
          <a data-aos="fade-up" href="https://www.linkedin.com/in/radhika-mukherji/" target="_blank"> Linkedin <i class="fab fa-linkedin"></i> </a>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-12">
          <h3 data-aos="fade-up">Amar Pandit</h3>
          <h4 data-aos="fade-up">Co-Founder & CFO</h4>
          <p data-aos="fade-up">Amar Pandit is the co-founder of Happydemic and also a Certified Financial Planner with CFA Charterholder from the CFA Institute in Virginia, USA. He believes in spreading financial literacy and has already written 4 bestselling books and 750+ columns in leading publications and portals. He is very passionate about music & movies. He has also produced a Marathi film ‘Cycle’.</p>
          <a data-aos="fade-up" href="https://www.linkedin.com/in/amar-pandit-cfa-cfp-66a478/" target="_blank"> Linkedin <i class="fab fa-linkedin"></i> </a>
        </div>
        <div class="col-sm-6 col-12">
          <img data-aos="fade-up" src="img/leadership/amar pandit.png" alt="">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-12">
          <img data-aos="fade-up" src="img/leadership/lokesh bhagwat.png" alt="">
        </div>
        <div class="col-sm-6 col-12">
          <h3 data-aos="fade-up">Lokesh Bhagwat</h3>
          <h4 data-aos="fade-up">Chief Strategic Relations Officer</h4>
          <p data-aos="fade-up">Lokesh is an Entrepreneur at heart and the CTO, Chief Strategic Relations Officer at Happydemic. He has an enviable track record of building business units in emerging verticals and in international geographies like USA & Japan. Lokesh has held senior leadership position in India and in USA at leading IT services companies like Patni, Mastek, Blue Star. His knowledge of music & life skills and combining them both, makes him a force to reckon with.</p>
          <a data-aos="fade-up" href="https://www.linkedin.com/in/lokeshbhagwat/" target="_blank"> Linkedin <i class="fab fa-linkedin"></i> </a>
        </div>

      </div>
      <div class="row">
        <div class="col-sm-6 col-12">
          <h3 data-aos="fade-up">Narayan Ranjan</h3>
          <h4 data-aos="fade-up">Chief Operating Officer</h4>
          <p data-aos="fade-up">Narayan Ranjan is an accomplished business professional with well-rounded & noticeable leadership experience of 18 years in large & complex organizations like Star TV Network & Viacom18. With close to 3 decades of work experience, Narayan has the proven ability to establish, develop, lead and manage businesses – in fast-moving, rapidly growing, highly challenging, multinational and multicultural organizations.</p>
          <a data-aos="fade-up" target="_blank" href="https://www.linkedin.com/in/npranjan/" > Linkedin <i class="fab fa-linkedin"></i> </a>
        </div>
        <div class="col-sm-6 col-12">
          <img data-aos="fade-up" src="img/leadership/narayan ranjan.png" alt="">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-12">
          <img data-aos="fade-up" src="img/leadership/sudha shirodkar.png" alt="">
        </div>
        <div class="col-sm-6 col-12">
          <h3 data-aos="fade-up">Sudha Shirodkar</h3>
          <h4 data-aos="fade-up">Vice-President Business Develpoment</h4>
          <p data-aos="fade-up">A writer-at-heart, Sudha is trained in Carnatic music. Her diverse journey spans 25 creative years in Advertising and FM Radio. Her passion for music, people & relationships makes her an integral part of Happydemic. She leads Business Development at Happydemic and enjoys helping each customer organisation find the perfect solution to their challenge.</p>
          <a data-aos="fade-up" href="https://www.linkedin.com/in/sudhashirodkar9/" target="_blank"> Linkedin <i class="fab fa-linkedin"></i> </a>
        </div>

      </div>
      <div class="row">
        <div class="col-sm-6 col-12">
          <h3 data-aos="fade-up">Samipa Shaw</h3>
          <h4 data-aos="fade-up">Vice President Artist Management </h4>
          <p data-aos="fade-up">Samipa Jolly Shaw has brought with her over a decade’s worth of invaluable experience of the Aviation and Hospitality industry to this company as the VP. Her leadership skills and keen interest in management make her an asset for Happydemic. Her ability to capitalise on her creativity and her love for music are her biggest strengths. Her innovative ideas help artists to forge into the future as confident, fearless and positive beings.</p>
          <a data-aos="fade-up" href="https://www.linkedin.com/in/samipa-jolly-shaw-30007b13a/" target="_blank"> Linkedin <i class="fab fa-linkedin"></i> </a>
        </div>
        <div class="col-sm-6 col-12">
          <img data-aos="fade-up" src="img/leadership/shamipa show.png" alt="">
        </div>
      </div>
    </div>
  </div>
  <div class="section35" id="gallery">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 data-aos="fade-up">Our Studio</h2>
          <div class="gallery owl-carousel owl-theme" data-slider-id="1">
            <div class="item" data-thumb='<img src="img/leadership/slider.png">'>
              <img src="img/leadership/slider.png">
              <div class="overlay"></div>
            </div>
            <div class="item" data-thumb='<img src="img/leadership/slider2.png">'>
              <img src="img/leadership/slider2.png">
              <div class="overlay"></div>
            </div>
            <div class="item" data-thumb='<img src="img/leadership/slider3.png">'>
              <img src="img/leadership/slider3.png">
              <div class="overlay"></div>
            </div>
            <div class="item" data-thumb='<img src="img/leadership/slider4.png">'>
              <img src="img/leadership/slider4.png">
              <div class="overlay"></div>
            </div>
            <div class="item" data-thumb='<img src="img/leadership/slider6.png">'>
              <img src="img/leadership/slider6.png">
              <div class="overlay"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.gallery').owlCarousel({
        stagePadding: 0,
        items: 1,
        loop: false,
        margin: 30,
        dots: false,
        autoplay: false,
        smartSpeed: 1500,
        nav: true,
        center: true,
        thumbs: true,
        thumbImage: true,
        thumbContainerClass: 'owl-thumbs',
        thumbItemClass: 'owl-thumb-item',
        navText: [
          "<i class='fas fa-chevron-left'></i>",
          "<i class='fas fa-chevron-right'></i>"
        ],
        responsive : {
          0 : {
              center: true,
          items: 1,
          loop:true,
          thumbs:false,
          center:true,
          thumbImage: false,
            thumbImage: false,
          margin: 5,
            nav: false,
          stagePadding:30,
          loop:true,

          // nav: true,
          },
          768 : {
          items: 1,
          }
        }
      });
      });
  </script>
</body>

</html>
