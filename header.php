<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>happydemic</title>
  <!-- MDB icon -->
  <link rel="icon" href="img/home/favicon.png" type="image/x-icon" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" />
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" />
  <!-- MDB -->
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" href="css/magnific-popup.css">
  <link rel="stylesheet" href="css/mdb.min.css" />
   <link rel="stylesheet" type="text/css" href="css/animate.css">
   <link href="css/aos.css" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css" />
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
        <a class="navbar-brand" href="index.php"><img src="img/home/logo.svg" alt=""> </a>
        <div class="collapse navbar-collapse" id="navbar">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="corporates.php">Corporates</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="artists.php">Artists</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="leadership.php">Leadership</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact-us.php">Contact Us</a>
            </li>
            <li class="nav-item popupmodal">
              <a class="nav-link popme" href="#login" data-effect="mfp-zoom-in">Login</a>
            </li>
          </ul>
        </div>
        <div class="button_container menu menu-icon openme">
          <span class="top"></span>
          <span class="middle"></span>
          <span class="bottom"></span>
        </div>
      </div>
    </nav>
  </header>

  <!-- Popup itself -->
  <div id="login" class="mfp-with-anim mfp-hide popup_modal_size">
    <div class="modal_popup">
      <div class="image_sec"></div>
      <div class="content_sec">
        <h2>Hop right into <br>
          our music factory!</h2>
        <a href="#" class="google"> <img src="img/user/google.png" alt=""> </a>
        <a href="#" class="facebook"> <img src="img/user/facebook.png" alt=""> </a>
        <h3> <span>or sign up with your email</span> </h3>
        <form action="/action_page.php">
          <div class="form-group">
            <label for="usr">Your Avatar/Name <span>*</span></label>
            <input type="text" class="form-control" id="name">
          </div>
          <div class="form-group">
            <label for="email">Email address <span>*</span></label>
            <input type="text" class="form-control" id="email">
          </div>
          <div class="form-group">
            <label for="password">Password <span>*</span></label>
            <input type="password" class="form-control" id="password">
          </div>
          <button type="button" class="btn btn-warning">Sign up</button>
        </form>
        <p>Dont’t have an account? <a href="#">Sign in now</a> </p>
      </div>
    </div>
  </div>

  <div class="menulink">
    <div class="menulink__content">
      <div class="menulink__list list-group" id="main-menu">
        <div class="button_container menu menu-icon closeme">
          <span class="top"></span>
          <span class="middle"></span>
          <span class="bottom"></span>
        </div>
        <h2>Menu</h2>
        <ul>
          <li>
            <a class="menulink__list-item" href="our-thinking.php">Blogs & Vlogs</a>
          </li>
          <li>
            <div class="menulink__list-item">
              <a class="dropdown_link" href="corporate-offerings.php">Corporate Offerings</a>
              <i class="fas fa-sort-up" data-mdb-toggle="collapse" aria-expanded="false" aria-controls="solution"  href="#solution"></i>
            </div>
            <div class="menulink__list-item1 collapse" id="solution">
              <a href="brand-essence.php" class="menulink__list-item">Brand Essence</a>
              <a href="revenue-impact.php" class="menulink__list-item">Revenue Impact</a>
              <a href="organisation-culture.php" class="menulink__list-item">Organisation & Culture</a>
            </div>
          </li>
          <li>
            <a class="menulink__list-item" href="solutions.php">Solutions</a>
          </li>
          <li>
            <a class="menulink__list-item" href="people.php">People</a>
          </li>
          <li>
            <a class="menulink__list-item" href="careers.php">careers</a>
          </li>
          <li>
            <a class="menulink__list-item" href="news.php">news</a>
          </li>
          <li class="desktop_none mobile_menu popupmodal">
            <a class="menulink__list-item" href="corporates.php">Corporates</a>
            <a class="menulink__list-item" href="artists.php">Artists</a>
            <a class="menulink__list-item" href="leadership.php">Leadership</a>
            <a class="menulink__list-item" href="contact-us.php">Contact Us</a>
            <a class="menulink__list-item login_btn popme" href="#login" data-effect="mfp-zoom-in">Login</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
