<?php include 'header.php';?>
  <!-- Start your project here-->
  <div class="section31 container">
    <div class="row">
      <div class="col-12">
        <h2 data-aos="fade-up">We believe in</h2>
        <p data-aos="fade-up">Collaboration. Innovation. Excellence. Passion.<br> Humility. Continuous Learning & Improvement.<br> Leadership. Long-term Relationship. </p>
        <img data-aos="fade-up" class="mobile_none" src="img/team/banner.png" alt="">
        <img data-aos="fade-up" class="desktop_none" src="img/team/banner_mobile.png" alt="">
      </div>
      <div class="col-12 text_field">
        <p data-aos="fade-up">Happydemic is a crucible, in which each of us achieve far more than our capabilities. </p>
        <h4 data-aos="fade-up">We unleash creativity!!</h4>
      </div>
    </div>
  </div>
  <div class="section32 container">
    <div class="row">
      <div class="col-12">
        <h2 data-aos="fade-up">Meet our Team</h2>
        <div class="team owl-carousel owl-theme">
          <div class="item">
            <ul>
              <li data-aos="fade-up">
                <a target="_blank" href="https://www.linkedin.com/in/amir-khan-697687137/">
                  <img src="img/team/3.png" alt="">
                  <h3>Amir Khan</h3>
                  <p>Client Servicing</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
              <li data-aos="fade-up">
                <a href="https://www.linkedin.com/in/arva-kagalwala-6a1302115/" target="_blank" >
                  <img src="img/team/10.png" alt="">
                  <h3>Arva Kagalwala</h3>
                  <p>Artist Management</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
              <li data-aos="fade-up">
                <a href="https://www.linkedin.com/in/purvi-aklujkar/" target="_blank" >
                  <img src="img/team/7.png" alt="">
                  <h3>Purvi Aklujkar</h3>
                  <p>Associate Vice President <br>
                    Business Development</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="item">
            <ul>
              <li data-aos="fade-up">
                <a href="https://www.linkedin.com/in/anjali-deshprabhu-270936160/" target="_blank" >
                  <img src="img/team/9.png" alt="">
                  <h3>Anjali Deshprabhu</h3>
                  <p>SongStruck</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
              <li data-aos="fade-up">
                <a href="https://www.linkedin.com/in/trupti-rahate-755860122/" target="_blank" >
                  <img src="img/team/5.png" alt="">
                  <h3>Trupti Rahate</h3>
                  <p>SongStruck</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
              <li data-aos="fade-up">
                <a href="https://www.linkedin.com/in/sagarpsawant/" target="_blank" >
                  <img src="img/team/1.png" alt="">
                  <h3>Sagar Sawant</h3>
                  <p>Business Development</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="item">
            <ul>
              <li data-aos="fade-up">
                <a href="https://www.linkedin.com/in/ajinkya-rege-42641056/" target="_blank" >
                  <img src="img/team/8.png" alt="">
                  <h3>Ajinkya Rege</h3>
                  <p>SongStruck</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
              <li data-aos="fade-up">
                <a href="https://www.linkedin.com/in/javed-tank-7796051a/" target="_blank" >
                  <img src="img/team/2.png" alt="">
                  <h3>Javed Tank</h3>
                  <p>Creatives</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
              <li data-aos="fade-up">
                <a href="https://www.linkedin.com/in/saviola-monteiro-858683137/" target="_blank" >
                  <img src="img/team/11.png" alt="">
                  <h3>Saviola Monteiro</h3>
                  <p>Executive Assitant to CEO</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="item">
            <ul>
              <li data-aos="fade-up">
                <a href="https://www.linkedin.com/in/arva-boxwala-2b77a7147/" target="_blank" >
                  <img src="img/team/12.png" alt="">
                  <h3>Arva Boxwala</h3>
                  <p>SongStruck</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
              <li data-aos="fade-up">
                <a href="https://www.linkedin.com/in/nirzaribhide/" target="_blank" >
                  <img src="img/team/4.png" alt="">
                  <h3>Nirzari Bhide</h3>
                  <p>Business Development</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li>
              <!-- <li data-aos="fade-up">
                <a href="https://www.linkedin.com/in/trupti-rahate-755860122/" target="_blank" >
                  <img src="img/team/5.png" alt="">
                  <h3>Trupti Rahate</h3>
                  <p>SongStruck</p>
                  <i class="fab fa-linkedin"></i>
                </a>
              </li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.team').owlCarousel({
        stagePadding: 0,
        items: 4,
        loop: false,
        margin: 30,
        dots: false,
        autoplay: false,
        smartSpeed: 1500,
        autoHeight:true,
        nav: true,
        navText: [
          "<img src='img/team/left_arrow.svg'>",
          "<img src='img/team/right_arrow.svg'>"
        ],
        responsive : {
            0 : {
            items: 1,
            },
            768 : {
            items: 3,
            }
        }
      });
      });

  </script>
</body>

</html>
