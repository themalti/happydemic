<?php include 'header.php';?>
  <!-- Start your project here-->
  <div class="section48">
    <div class="container">
      <div class="row">
        <div class="col">
          <h4 class="color-yellow" data-aos="fade-up">In the news</h4>
        </div>
      </div>
    </div>
  </div>
  <div class="section49">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 data-aos="fade-up">Recent articles</h2>
          <a class="main_div" href="https://www.firstpost.com/business/want-a-singer-to-croon-on-your-birthday-get-one-vetted-by-singer-shaans-start-up-happydemic-2727118.html " target="_blank">
            <div class="left_img">
                <img data-aos="fade-up" src="img/news/bestmedia_banner.png" class="card-img-top" alt="..." />
            </div>
            <div class="right_text">
              <img data-aos="fade-up" src="img/news/firstpost.jpg" alt="">
              <span data-aos="fade-up" class="desktop_none">April 13, 2016</span>
              <h3 data-aos="fade-up">Want a singer to croon on your birthday? Get one vetted by singer Shaan’s start-up, Happydemic.</h3>
              <p data-aos="fade-up" class="mobile_none">April 13, 2016</p>
            </div>
          </a>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <img data-aos="fade-up" src="img/news/rm_banner.png" alt="">
            <a href="https://www.entrepreneur.com/article/333905" target="_blank">
                  <div class="mask"></div>
              </a>
            <div class="card-body">
              <h5 data-aos="fade-up" class="card-title"> <img src="img/news/entrepreneur.png" alt=""> <span>July 6, 2017</span> </h5>
              <p data-aos="fade-up" class="card-text">
                Inside the office of Radhika Mukherji, owner of Happydemic.
              </p>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card">
            <img data-aos="fade-up" src="img/news/yourstory_banner3.png" class="card-img-top" alt="..." />
            <a href="https://timesofindia.indiatimes.com/entertainment/hindi/music/news/our-company-is-giving-emerging-artistes-a-new-platform-radhika-mukherji/articleshow/60807769.cms " target="_blank">
                  <div class="mask"></div>
              </a>
            <div class="card-body">
              <h5 data-aos="fade-up" class="card-title"> <img src="img/news/toi.png" alt=""> <span>September 24, 2017</span> </h5>
              <p data-aos="fade-up" class="card-text">
                Our company is giving emerging artistes a new platform: Radhika Mukherji
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section50">
    <div class="container">
      <div class="row">
        <div class="col">
          <h2 data-aos="fade-up">Find more articles</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-12" data-aos="fade-up">
          <a href="https://indianceo.in/startup/radhika-mukherji-happydemic-shaan-wife/ " target="_blank" data-aos="fade-up">
            <div class="img_sec">
              <img src="img/news/indian.png" alt="">
            </div>
            <div class="content_sec">
              <p>January 2, 2017</p>
              <h3>Digital Interaction with Celebrity Wife and Entrepreneur Radhika Mukherji, Founder & CEO of Happydemic.</h3>
            </div>
            <div class="arrow_sec">
              <img src="img/news/arrow.svg" alt="">
            </div>
          </a>
        </div>
        <div class="col-12" data-aos="fade-up">
          <a href="https://www.hindustantimes.com/more-lifestyle/live-street-gig-for-mumbaikars-a-reason-to-celebrate-mumbai/story-n4zNX1J0lIEjogkQOQPMxN.html" target="_blank" data-aos="fade-up">
            <div class="img_sec">
              <img src="img/news/hindustan-times.jpg" alt="">
            </div>
            <div class="content_sec">
              <p>October 23, 2016</p>
              <h3>Live street gig for Mumbaikars: A reason to celebrate Mumbai</h3>
            </div>
            <div class="arrow_sec">
              <img src="img/news/arrow.svg" alt="">
            </div>
          </a>
        </div>

        <div class="col-12" data-aos="fade-up">
          <a href="https://www.exchange4media.com/marketing-news/hum-hein-muthoot-anthem-is-an-attempt-to-bring-oneness-among-company-employees-94461.html" target="_blank" data-aos="fade-up">
            <div class="img_sec">
              <img src="img/news/exchange.png" alt="">
            </div>
            <div class="content_sec">
              <p>February 6, 2019</p>
              <h3>‘Hum Hein Muthoot’ anthem is an attempt to bring oneness among company employees</h3>
            </div>
            <div class="arrow_sec">
              <img src="img/news/arrow.svg" alt="">
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.gallery').owlCarousel({
        stagePadding: 0,
        items: 2,
        loop: false,
        margin: 30,
        dots: true,
        autoplay: false,
        smartSpeed: 1500,
        nav: false,
      });
      });
  </script>
</body>

</html>
