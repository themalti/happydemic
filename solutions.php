<?php include 'header.php';?>

  <!-- Start your project here-->
  <div class="section51">
    <div class="container">
      <div class="row">
        <div class="col-sm-7">
          <img data-aos="fade-up" src="img/casestudy-landing/5.svg" alt="" class="number">
          <h3 data-aos="fade-up">of challenging the status<br> quo and transforming<br> engagement </h3>
          <p data-aos="fade-up">Find out how we help some of the India’s biggest companies<br> with deep musical engagement and impact. </p>
        </div>
        <div class="col-sm-5">
          <img data-aos="fade-up" src="img/casestudy-landing/banner.png" alt="" class="banner mobile_none">
          <img data-aos="fade-up" src="img/casestudy-landing/banner_mobile.png" alt="" class="banner desktop_none">
        </div>
      </div>
    </div>
  </div>

  <div class="section52 container">
    <div class="row">
      <div class="col">
        <h2 data-aos="fade-up">Our Offerings</h2>
      </div>
    </div>
    <div class="row popupmodal">
      <div class="col" id="music_video">
        <a href="#songstruck_vid" data-effect="mfp-zoom-in" data-aos="fade-up" class="popme video_sec_large seven">
          <div class="text_sec">
            <img src="img/casestudy-landing/play.png" alt="">
            <h4>Songstruck </h4>
            <h5>A 20 minutes hyper-personalised musical experience delivered at home or via video call for your stakeholders. Helps you win hearts.</h5>
          </div>
        </a>
        <a href="#musicalt_vid" data-effect="mfp-zoom-in" data-aos="fade-up" class="popme video_sec three">
          <div class="text_sec">
            <img src="img/casestudy-landing/play.png" alt="">
            <h4>Musical Talent Hunt</h4>
            <h5>An initiative that helps to identify & nurture innate talent within your stakeholders. Helps build communities.</h5>
          </div>
        </a>

        <a href="#themesong_vid" data-effect="mfp-zoom-in" data-aos="fade-up" class="popme video_sec four">
          <div class="text_sec">
            <img src="img/casestudy-landing/play.png" alt="">
            <h4>Theme Songs</h4>
            <h5>Promote Products. Enhance Motivation. Amplify Brands.</h5>
          </div>
        </a>
        <div class="clear_both desktop_none"></div>
        <a  data-effect="mfp-zoom-in" data-aos="fade-up" class="popme video_sec_large six">
          <div class="text_sec">
            <!-- <img src="img/casestudy-landing/play.png" alt=""> -->
            <h4>Sonic Sounds</h4>
            <h5>Signature Tunes are intended to create strong brand recall especially in categories where there is immense competition like banking, FMCG and telecom.</h5>
          </div>
        </a>
        <a  href="#musiclive"  data-aos="fade-up" class="popme video_sec_large eight">
          <div class="text_sec">
            <img src="img/casestudy-landing/play.png" alt="">
            <h4>Musical Live Experiences</h4>
            <h5>A basket of theme-based interactive musical experiences to bring in high level of engagement to lift up the spirits!</h5>
          </div>
        </a>

        <a href="#musicalgame_vid" data-aos="fade-up"  class=" popme video_sec two">
          <div class="text_sec">
            <img src="img/casestudy-landing/play.png" alt="">
            <h4>Musical Games </h4>
            <h5>Interactive and participative music-based games designed to your needs.</h5>
          </div>
        </a>

        <a href="#music_reel" data-effect="mfp-zoom-in" data-aos="fade-up" class="popme video_sec one">
          <div class="text_sec">
            <img src="img/casestudy-landing/play.png" alt="">
            <h4>Musical Reel </h4>
            <h5>A musical montage capturing the highlights of ones life story. Flatter them!</h5>
          </div>
        </a>
          <div class="clear_both desktop_none"></div>
        <a href="#bespok_vid" data-effect="mfp-zoom-in" data-aos="fade-up" class="popme video_sec_large five">
          <div class="text_sec">
            <img src="img/casestudy-landing/play.png" alt="">
            <h4>Bespoke Solutions</h4>
            <h5>Add  more sparkle and fun & frolic to your wedding with customized wedding invitations, couple songs, invitation reels,  bands for your varied functions </h5>
          </div>
        </a>

      </div>
    </div>
  </div>
  <div id="songstruck_vid" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/5tk9CvBhLKU?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <div id="musicalt_vid" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/OlvsTVlDj5A?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <div id="themesong_vid" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/_kEPgWhgg2Y?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <div id="sonicsong_vid" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/_kEPgWhgg2Y?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <div id="musicalgame_vid" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/bXy8suhwNUM?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <div id="music_reel" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/3lMYv8K4Mqk?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <div id="bespok_vid" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/CIO1cUGVGO8?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <div id="musiclive" class="mfp-with-anim popup_modal_size mfp-hide corp">
    <div class="modal_popup ">
      <iframe width="100%" height="600" src="https://www.youtube.com/embed/zis0TC2Z1dM?rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
  <!-- End your project here-->
<?php include 'footer.php';?>

</body>

</html>
