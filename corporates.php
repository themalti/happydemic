<?php include 'header.php';?>
  <!-- Start your project here-->

  <div class="section12">
    <div class="container">
      <div class="row">
        <div class="col">
          <h2 data-aos="fade-up">Make music an integral part of your <br> organisation strategy.</h2>
          <p data-aos="fade-up">Unleash the power of music for innovative engagement <br>solutions for all your stakeholders.</p>
          <a data-aos="fade-up" href="corporate-offerings.php">Corporate Offerings</a>
        </div>
      </div>
    </div>
  </div>
  <div class="section15 container">
    <div class="row">
      <div class="col popupmodal" >
        <h2 data-aos="fade-up">Integrating music with organisation goals for positive business impact</h2>
          <a class="videowatch popme" href="#watchvideo" data-effect="mfp-zoom-in">
            <img src="img/casestudy/playbutton.svg" alt="">
          </a>
        <div class="organisation owl-carousel owl-theme" >
          <div class="item">
            <img src="img/corporate/slider.png" alt="">
            <div class="text_sec1">
              <h3>Why people love music?</h3>
              <p>Music releases DOPAMINE in the reward center of the brain.</p>
            <p>  It is the same chemical released when you eat your favourite food!</p>
            <p>  Dopamine can help improve FOCUS.</p>
            </div>
          </div>
          <div class="item">
              <img src="img/corporate/slider2.png" alt="">
              <div class="text_sec2">
                <h3>How music make a positive <br>
                    impact on our health?</h3>
                    <p>Reduces stress & anxiety<br>
                    Decreases Pain<br>
                    Improves Immune Function<br>
                    Aids Memory<br>
                    Increases Memory</p>
              </div>
          </div>
          <div class="item">
            <img src="img/corporate/slider3.png" alt="">
            <div class="text_sec3">
              <h3>How music affects<br>
                  your brain?</h3>
                  <p>Music activates the motor cortex causing you<br>
                  to tap your foot to the song</p>
                  <p>Music stimulates emotion for the nucleus<br>
                  accumbens, amygdala and cerebellum</p>
                  <p>Music is processed in the auditory cortex</p>
                  <p>Music stimulates memories from the<br>
                  hippocampus</p>
                  <p>Song lyrics activate Broca’s & Wernicke’s<br>
                  area, the parts of our brains that process language.</p>
            </div>
          </div>
          <div class="item">
            <img src="img/corporate/slider4.png" alt="">
            <div class="text_sec4">
              <h3>Positive impact on our work and for the organisations</h3>
              <p>Improved decision-making skills<br>
              Opportunities for creative self expression<br>
            Increased self-confidence<br>
          Improved self-esteem</p>
            </div>
          </div>
          <div class="item">
            <img src="img/corporate/slider5.png" alt="">
            <div class="text_sec5">
              <h3>Better business outcomes</h3>
              <p>77% of small and medium sized business owners<br>
                believe music improves employee morale.</p>
                <p>65% of business owners<br> believe music makes employees more productive</p>
                <p>40% of business owners believe playing music<br> increases  sales</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section14 container">
    <div class="row">
      <div class="col">
        <div class="background_dot"></div>
        <div class="youtube_sec" data-aos="zoom-up">
          <div class="youtube_play">
            <!-- <img src="img/corporate/play_btn.png" alt=""> -->
          </div>
        </div>
        <div class="youtube_text" >
          <h2 data-aos="fade-up">A strong and connecting sense of purpose has always been essential for maximizing business potential.
Communicating the purpose is crucial to unite teams and empower them to achieve best results. Music has the power to connect and unite people to drive uncommon growth for organizations in these uncertain times.  </h2>
          <p data-aos="fade-up">- Radhika Mukherji, CEO</p>
        </div>
      </div>
    </div>
  </div>
  <div class="section13 container">
    <div class="row">
      <div class="col-sm-6">
        <h2 data-aos="fade-up">Our Goal is to help<br> you reach yours! </h2>
        <img data-aos="zoom-up" src="img/corporate/flow.svg" alt="">
      </div>
      <div class="col-sm-6">
        <p data-aos="fade-up">At Happydemic, we persuade your employees to be themselves at the workplace to inspire
          them to seamlessly blend their organizational goals with their personal goals.
        </p>
        <p data-aos="fade-up">You can now nurture and strengthen relationships amongst your employees through our music-empowered innovations in engagement solutions to reap the following benefits:-</p>
        <ul>
          <li data-aos="fade-up">Grow Business</li>
          <li data-aos="fade-up">Boost Sales</li>
          <li data-aos="fade-up">Improve Employee Engagement</li>
          <li data-aos="fade-up">Improve Retention Rate</li>
          <li data-aos="fade-up">Boost Revenue Impact</li>
          <li data-aos="fade-up">Strengthen Brand Essence</li>
          <li data-aos="fade-up">Create Effective Organisating and Culture</li>
          <li data-aos="fade-up">Address a variety of frictions owing to the
            multi-generational workforce.
          </li>
          <li data-aos="fade-up">Tackle low productivity and low morale
            challenges.
          </li>
          <li data-aos="fade-up">Minimize differences amongst cross-
            functional work teams.
          </li>
          <li data-aos="fade-up">Shrink communication gaps resulting from
            geographical barriers. </li>
        </ul>
      </div>
    </div>
  </div>


  <div class="section16 container">
    <div class="row">
      <div class="col">
        <h2 data-aos="fade-up">Our capabilities are shaped <br>
          by your organisational challenges </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
    <p data-aos="fade-up">Given the multitude of music-inspired solutions we’ve provided to clients across different sectors, numerous organisations count on our capabilities to crack some of their most complex challenges. Happydemic plays a vital role in ensuring that every solution leaves behind a deep impact amongst the client’s key strategic constituents. </p>
      </div>
      <div class="col-sm-6">
    <p data-aos="fade-up">Every solution that we provide, fosters positive conversations thereby boosting employee engagement across the entire workplace. Right from stimulating creative ideation in employees, to enhancing their productivity levels, the role of Happydemic has transcended beyond live events and entertainment. </p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <div class="accordion accordion-flush" id="accordionCashStudy" data-aos="fade-up">
          <div class="accordion-item one_back">
            <h2 class="accordion-header" id="flush-headingOne">
              <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">Revenue Impact </button>
            </h2>
            <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-mdb-parent="#accordionCashStudy">
              <div class="accordion-body">
              Music-inspired activities involve multitude parts of your brain encompassing emotional, cognitive and motor areas. Promote intuitive bonding mechanisms amongst your employees to boost revenue impact through our remarkable musical engagement solutions.
                <a href="revenue-impact.php#revenue-impact" class="cash_study">Read Case Study <img src="img/corporate/right_blue.svg" alt=""> </a>
              </div>
            </div>
          </div>
          <div class="accordion-item two_back">
            <h2 class="accordion-header" id="flush-headingTwo">
              <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
            Brand Essence
              </button>
            </h2>
            <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-mdb-parent="#accordionCashStudy">
              <div class="accordion-body">
              Helping your brand forge a strong emotional bond with your customers leads to improved business performance and deeper customer engagement. We help embed your brand with a strong musical engagement strategy combined with other branding efforts to create a consistent image in your clients' mind about who you are, what you do and what you stand for.
                <a href="brand-essence.php#brand-essence" class="cash_study">Read Case Study <img src="img/corporate/right_blue.svg" alt=""> </a>
              </div>
            </div>
          </div>
          <div class="accordion-item three_back">
            <h2 class="accordion-header" id="flush-headingThree">
              <button class="accordion-button collapsed" type="button" data-mdb-toggle="collapse" data-mdb-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
          Organisation and Culture
              </button>
            </h2>
            <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-mdb-parent="#accordionCashStudy">
              <div class="accordion-body">
              If your belief resonates with our belief that human assets are not meant to be treated like robots, let our music-infused innovations in engagement transform your organisational culture oozing with enthusiasm and positivity.
                <a href="organisation-culture.php#organisation-culture" class="cash_study">Read Case Study <img src="img/corporate/right_blue.svg" alt=""> </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section11">
    <div class="container">
      <div class="row">
        <div class="col">
          <h4 data-aos="fade-up"><span>Discover more about us</span> </h4>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="card" data-aos="fade-up">
            <img src="img/home/discover1.png" class="card-img" alt="..." />
            <a href="leadership.php" class="card-img-overlay">
              <h5 class="card-title">About Us<img src="img/home/right_arrow.svg" alt=""></h5>
            </a>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="card " data-aos="fade-up" data-aos-delay="900">
            <img src="img/home/discover2.png" class="card-img" alt="..." />
            <a href="leadership.php#gallery" class="card-img-overlay">
              <h5 class="card-title">Our Studio <img src="img/home/right_arrow.svg" alt=""></h5>
            </a>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="card " data-aos="fade-up" data-aos-delay="1000">
            <img src="img/home/discover3.png" class="card-img" alt="..." />
            <a href="our-thinking.php" class="card-img-overlay">
              <h5 class="card-title">Blogs & Vlogs <img src="img/home/right_arrow.svg" alt=""></h5>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="watchvideo" class="mfp-with-anim mfp-hide popup_modal_size illi">
    <div class="modal_popup">
      <video id="watchvideocorporate" mute>
        <source src="audio/corporate.mp4" type="video/mp4">
      </video>
      <button title="Close (Esc)" type="button" class="mfp-close closevideo">×</button>
    </div>
  </div>
  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.videowatch').on('click', function() {
        $('#watchvideocorporate')[0].play();
      });
      $('.closevideo').on('click', function() {
        $('#watchvideocorporate')[0].pause();
      });

      $('.organisation').owlCarousel({
        stagePadding: 0,
        items: 1,
        loop: false,
        margin: 0,
         autoHeight:true,
        dots: false,
        autoplay: false,
        smartSpeed: 800,
        nav: true,
        navText: [
          "<img src='img/corporate/left.svg'>",
          "<img src='img/corporate/right.svg'>"
        ],
      });
      });
  </script>
</body>

</html>
