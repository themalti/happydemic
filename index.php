<?php include 'header.php';?>
<?php 
  require('config.php');

  if ($db->connect_error) {
      die("Connection failed: " . $db->connect_error);
  }

  try{
      $result = mysqli_query($db,"SELECT b.*,DATE_FORMAT(b.created_at, '%d %M %Y') as created_at,bi.blog_thumbnail, bi.blog_banner, bi.middle_image FROM blogs as b left join blog_images as bi on bi.blog_id=b.id order by id desc");
      
     
      if (!empty($db->error)){
          throw new Exception();
      }
      
      $blogsResult = [];

      while ($row = mysqli_fetch_assoc($result)) {
         
          array_push($blogsResult, $row);
      }


  }catch(PDOException $exception){
      var_dump($db->error,'dd');
      die('ERROR: ' . $exception->getMessage());
  }
?>
  <!-- Start your project here-->
  <div class="section1 container">
    <div class="text_sec">
      <div class="home_banner owl-carousel owl-theme">
        <div class="item">
          <h2> <span>Happydemic</span> </h2>
          <h3> <span>Verb </span> The act of spreading happiness in the lives
          of artists & customers</h3>
          <h3><span>Plural</span> Does not exist </h3>
        </div>
        <div class="item">
          <h2>Music <span>moves</span> people.</h2>
          <h3>It can inspire, provoke, change or reassure.
            We believe it can do the same for your business.</h3>
        </div>
    </div>
      <div class="slider_top">
        <div class="slider_sec owl-carousel owl-theme">
          <!-- <div class="item">
            <ul class="first">
              <li><img src="img/home/1.jpg" alt="songstruck"> </li>
              <li>
                 <video autoplay muted loop>
                  <source src="img/home/4.mp4" type="video/mp4">
                </video>
              </li>
            </ul>
            <ul class="second">
              <li>
                 <video autoplay muted loop>
                  <source src="img/home/3.mp4" type="video/mp4">
                </video>
              </li>
              <li><img src="img/home/2.jpg" alt="songstruck"> </li>
            </ul>
          </div> -->
          <div class="item">
            <ul class="first">
              <li><img src="img/home/1.png" alt="songstruck"> </li>
              <li><img src="img/home/2.gif" alt="songstruck"> </li>
            </ul>
            <ul class="second">
              <li><img src="img/home/3.png" alt="songstruck"> </li>
              <li><img src="img/home/4.gif" alt="songstruck"> </li>
            </ul>
          </div>
        </div>

      </div>
      <a data-aos="fade-up" href="#" class="play_button"> <img src="img/home/play_button.svg" alt="songstruck"> </a>

      <div class="songstruck"  data-aos="fade-up">
        <div class="text_sec">
          <p data-aos="fade-up">Our composed track</p>
          <h4 data-aos="fade-up">Happydemic Tune </h4>
          <a data-aos="fade-up" href="https://soundcloud.com/happydemic_mike/sets/happydemic/s-PGvsxygOpOR" target="_blank" >Visit our playlist on SoundCloud </a>
          <div data-aos="fade-up" class="audio_section">
            <audio controls>
              <source src="audio/anthem.mp3"  type="audio/mp3">
            </audio>
            <div class="audio-player">
              <div class="timeline">
                <div class="progress"></div>
              </div>
              <div class="controls">
                <div class="play-container">
                  <div class="toggle-play play">
                </div>
                </div>
                <div class="time">
                  <div class="current">0:00</div>
                  <div class="divider">/</div>
                  <div class="length"></div>
                </div>
                <div class="volume-container">
                  <div class="volume-button">
                    <i class="fas volume  fa-volume-up"></i>
                    <div class="volume icono-volumeMedium"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="img_sec" data-aos="fade-up">
          <img src="img/home/disc.png" alt="songstruck" class="disc">
          <img src="img/home/disc_support.png" alt="songstruck" class="disc_support" >
        </div>
      </div>
    </div>
  </div>

 <div class="section2 container">
    <div class="text_sec"  data-aos="fade-up">
      <img data-aos="fade-up" src="img/home/truck.svg" alt="songstruck">
      <h2 data-aos="fade-up">A concert-like experience <br>
in the comfort of your home. </h2>
      <p data-aos="fade-up">Make every special moment a musical moment. Bring a hyper-personalized concert-like feel, home. <br><strong>#GiftAConcert</strong> </p>
      <a data-aos="fade-up" href="songstruck.php" class="btn btn-warning">Book Songstruck </a>
    </div>

    <div class="video_play" data-aos="fade-up">
      <div class="video-foreground">
        <video id="video_sec">
          <source src="audio/songstruck.mp4" type="video/mp4">
        </video>
      </div>
      <button type="button" name="button" id="play_youtube_btn">
        <img src="img/home/play.svg" alt="playbtn">
        <img src="img/home/pause.svg" alt="pausebtn">
       </button>
    </div>
  </div>
<div class="section2_2 container">
<div class="row">
  <div class="col">
    <h2 data-aos="fade-up">Occasions</h2>
  </div>
</div>
<div class="row desktop_none">
  <div class="col">
    <ul data-aos="fade-up">
      <li>Birthdays</li>
      <li>Anniversary</li>
      <li>Congratulations</li>
      <li>Get well soon</li>
      <li>Miss you</li>
      <li>Proposal</li>
    </ul>
  </div>
</div>
<div class="row">
  <div class="col-sm-4" data-aos="fade-up">
    <img src="img/home/o1.png" alt="">
  </div>
    <div class="col-sm-4" data-aos="fade-up">
      <img src="img/home/o2.png" alt="">
  </div>
  <div class="col-sm-4" data-aos="fade-up">
    <img src="img/home/o3.png" alt="">
  </div>
</div>
<div class="row mobile_none">
  <div class="col">
    <ul data-aos="fade-up">
      <li>Birthdays</li>
      <li>Anniversary</li>
      <li>Congratulations</li>
      <li>Get well soon</li>
      <li>Miss you</li>
      <li>Proposal</li>
    </ul>
  </div>
</div>
</div>
  <div class="section5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="gry_text" data-aos="fade-up">OUR SOLUTIONS</div>
          <h2 data-aos="fade-up">Music is the <span class="color-yellow">powerful incubator</span> <br>
            for human emotions. </h2>
          <p data-aos="fade-up">And we deploy this insight for all our customers to unlock progress by focussing on 3 key areas.</p>
        </div>
        <div class="col-sm-4">
          <a href="corporate-offerings.php" class="card" data-aos="fade-up" data-aos-delay="800">
            <img src="img/home/blog_1.png" class="card-img" alt="..." />
            <div class="card-img-overlay">
              <div class="text_title">
                <h5 class="card-title">Corporates</h5>
                <p>Brand Essence, Revenue Impact,<br>
                  Organisation & Culture</p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-4">
          <a href="songstruck.php" class="card" data-aos="fade-up" data-aos-delay="900">
            <img src="img/home/blog_2.png" class="card-img" alt="..." />
            <div class="card-img-overlay">
              <div class="text_title">
                <h5 class="card-title"> Individuals</h5>
                <p>Special Occasions, Celebrations, Gifting</p>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-4">
          <a href="artists.php" class="card" data-aos="fade-up" data-aos-delay="1000">
            <img src="img/home/blog_3.png" class="card-img" alt="..." />
            <div class="card-img-overlay">
              <div class="text_title">
                <h5 class="card-title">Artists</h5>
                <p>Discover, Mentor, Promote</p>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="section3 container">
    <div class="row">
      <div class="col-sm-7">
        <div class="gry_text" data-aos="fade-up">OUR REACH</div>
        <h2 data-aos="fade-up">On a mission to<br> spread <span class="color-yellow">happiness</span> <br> through music.</h2>
        <p data-aos="fade-up">Happydemic is a social entrepreneurship, with an aim to make a happy difference in
        the lives of artistes and customers. Our core strength is music.<br>
        Music brings an abundance of happiness, joy & it helps in nurturing relationships.
        Experience music, just not hear it! </p>
        <a data-aos="fade-up" href="leadership.php" class="underline_link">About us</a>
      </div>
      <div class="col-sm-5">
        <img data-aos="zoom-out" src="img/home/ishan.png" alt="songstruck" class="ishan_img">
      </div>
    </div>
    <div class="row pading_mobile">
      <div class="col-sm-2 col-6">
        <div class="card" data-aos="fade-up" data-aos-delay="900">
          <img src="img/home/heart_logo.svg" class="card-img-top" alt="..." />
          <div class="card-body">
            <h5 class="card-title">1200+</h5>
            <p class="card-text">Heart-Winning<br>Experiences
            </p>
          </div>
        </div>
      </div>
      <div class="col-sm-2 col-6">
        <div class="card" data-aos="fade-up" data-aos-delay="1000">
          <img src="img/home/mic_logo.svg" class="card-img-top" alt="..." />
          <div class="card-body">
            <h5 class="card-title">2200+</h5>
            <p class="card-text">Artistes<br>Onboard
            </p>
          </div>
        </div>
      </div>
      <div class="col-sm-2 col-6">
        <div class="card" data-aos="fade-up" data-aos-delay="1100">
          <img src="img/home/map_logo.svg" class="card-img-top" alt="..." />
          <div class="card-body">
            <h5 class="card-title">560+</h5>
            <p class="card-text">Active<br>Locations
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="section3_2" data-aos="fade-up">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 data-aos="fade-up">Happymonials</h2>

        <ul class="nav nav-tabs mb-3" id="ex1" role="tablist" data-aos="fade-up">
          <li class="nav-item" role="presentation">
            <a class="nav-link active" id="ex1-tab-1" data-mdb-toggle="tab" href="#ex1-tabs-1" role="tab" aria-controls="ex1-tabs-1" aria-selected="true">Corporates</a>
          </li>
          <li class="nav-item" role="presentation">
            <a class="nav-link" id="ex1-tab-2" data-mdb-toggle="tab" href="#ex1-tabs-2" role="tab" aria-controls="ex1-tabs-2" aria-selected="false">Individuals</a>
          </li>
          <li class="nav-item" role="presentation">
            <a class="nav-link" id="ex1-tab-3" data-mdb-toggle="tab" href="#ex1-tabs-3" role="tab" aria-controls="ex1-tabs-3" aria-selected="false">Artistes</a>
          </li>
        </ul>
        <!-- Tabs navs -->

        <!-- Tabs content -->
        <div class="tab-content" id="ex1-content" data-aos="fade-up">
          <div class="tab-pane fade show active" id="ex1-tabs-1" role="tabpanel" aria-labelledby="ex1-tab-1">
            <div class="happymonials">
                <div class="item" >
                  <div class="testi_text">
                    <p> All I can say I enjoyed my best and this is probably the best ever event I have done in my entire 20 years career. So thank you Happydemic for
                      being such lovely partners!</p>
                      <h4>Mr. Satyadeep Mishra</h4>
                      <p>Head HR Digital services  <br> Reliance Jio</p>
                  </div>
                </div>
                <div class="item" >
                  <div class="testi_text">
                    <p>I had an amazing feedback from the team who were given this special treatment. Let me quote one employee “ Yesterday me and my family were overwhelmed by the surprise arranged by the company. It was a unique experience for which I would like to thank you and everyone who was a part of this planning. It felt great to be recognised in this special way. Thanks again</p>
                      <h4>Anup Purohit</h4>
                      <p>Chief Information Officer <br> Yes Bank</p>
                  </div>
                </div>
            </div>
          </div>
          <div class="tab-pane fade" id="ex1-tabs-2" role="tabpanel" aria-labelledby="ex1-tab-2">
            <div class="happymonials">
                <div class="item" >
                  <div class="testi_text">
                    <p>Till now we only use to arrange for customers but experiencing the same is really different experience. A day full of work, meetings, followed by a soothing and relaxing evening through e-SongStruck...Thanks Team Happydemic. It was awesome and struck the musical nostalgia  </p>
                    <h4>Sumit Khedkar</h4>
                    <p>Mumbai, India</p>
                  </div>
                </div>
                <div class="item" >
                  <div class="testi_text">
                  <p>It was a great experience to have live artist on digital platform for birthday celebration. Something different than ordinary celebrations. It has made my day special. Thanks Happydemic Team.</p>
                  <h4>Jayesh Makwana</h4>
                  <p>Mumbai, India</p>
                  </div>
                </div>
            </div>
          </div>
          <div class="tab-pane fade" id="ex1-tabs-3" role="tabpanel" aria-labelledby="ex1-tab-3">
            <div class="happymonials">
                <div class="item" >
                  <div class="testi_text">
                  <p>Hello everyone, I am Shubhashish. I am a singer and performer. I have been associated with Happydemic for about 2 years now. It is fun working with Happydemic. I have done SongStruck for them. It is great concept - performance wise for me. I have learnt a lot from it as well. Thank you Happydemic. Also thank you for continuing with the performances online even in the lockdown. It was a pretty good idea. It was a really new, fun experience. They really know how to keep their artists and their clients happy.</p>
                  <h4>Shubhashish Shelgaokar</h4>
                  <p>Guitarist, Mumbai</p>
                  </div>
                </div>
                <div class="item" >
                  <div class="testi_text">
                <p>Hi everyone, this is Vikrant and I am from Himachal Pradesh. I have been working with Happydemic for about 2 years now. It has been an amazing experience. The best thing about Happydemic is that they promote their artists so well and isn't that every artists' dream! Even in the lockdown they helped their artists with a steady inflow of money. Thank you so much Happydemic. I hope this partnership stays the same in the future as well. Thank you!</p>
                  <h4>Vikrant Rana</h4>
                  <p>Guitarist Mumbai</p>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

 <div class="section6">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="gry_text" data-aos="fade-up">OUR WORK</div>
          <h2 data-aos="fade-up">Our success stories</h2>
        </div>
      </div>
    </div>
    <div data-aos="fade-up">
      <div class="stories_slider owl-carousel owl-theme">
        <div class="item"  >
          <div class="card">
            <img src="img/home/stories1.png" class="card-img" alt="..." />
            <div class="card-img-overlay">
              <div class="text_section">
                <h5 class="card-title">Reliance JIO</h5>
                <p class="card-text">The power of music goes a long way in engaging and uniting a corporate. With a fun-filled talent-hunt, we created a win-win solution for management & employees.</p>
                <a href="organisation-culture.php" class="card-link">Read More</a>
              </div>
            </div>
            <a href="organisation-culture.php">
              <div class="mask" ></div>
            </a>
          </div>
        </div>

        <div class="item active" >
          <div class="card">
            <img src="img/home/stories2.png" class="card-img" alt="..." />
            <div class="card-img-overlay">
              <div class="text_section">
                <h5 class="card-title">Numeric UPS</h5>
                <p class="card-text">A motivational musical program crafted to bring about a change in the mindset amongst employees of Numeric UPS and infuse fresh energy amongst them through a brand new tagline - New Energy To Power </p>
                <a href="organisation-culture.php" class="card-link">Read More</a>
              </div>
            </div>
            <a href="organisation-culture.php">
              <div class="mask" ></div>
            </a>
          </div>
        </div>
        <div class="item">
          <div class="card">
            <img src="img/home/stories3.png" class="card-img" alt="..." />
            <div class="card-img-overlay">
              <div class="text_section">
                <h5 class="card-title">ACC</h5>
                <p class="card-text">A musically activated product launch for ACC Gold in a way never done before. Songstruck activity conducted with select dealers
across all their outlets : a big game-changing activation program in a commodity segment like cements. </p>
                <a href="brand-essence.php" class="card-link">Read More</a>
              </div>
            </div>
            <a href="brand-essence.php">
              <div class="mask" ></div>
            </a>
          </div>
        </div>
        <div class="item">
          <div class="card" data-aos="fade-up" data-aos-delay="1100">
            <img src="img/home/stories4.png" class="card-img" alt="..." />
            <div class="card-img-overlay">
              <div class="text_section">
                <h5 class="card-title">Axis Mutual Fund</h5>
                <p class="card-text">Axis Mutual Fund has been engaging with Institutional Financial Advisors (IFAs) in various ways.</p>
                <a href="revenue-impact.php" class="card-link">Read More</a>
              </div>
            </div>
            <a href="revenue-impact.php">
              <div class="mask" ></div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

 <div class="section7">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h4>Clients we’ve partnered with</h4>
          <div data-aos="fade-up">
            <div class="client_logo_slider owl-carousel owl-theme mobile_none" >
              <div class="item">
                <img src="img/home/sbi.png" alt="happydemic">
                <img src="img/home/sbi_general.png" alt="happydemic">
              </div>
              <div class="item">
                <img src="img/home/fadring.png" alt="happydemic">
                <img src="img/home/kotak.png" alt="happydemic">
              </div>
              <div class="item">
                  <img src="img/home/citibank.png" alt="happydemic">
                    <img src="img/home/pg.png" alt="happydemic">
              </div>
              <div class="item">
                  <img src="img/home/dbs.png" alt="happydemic">
                    <img src="img/home/kotak1.png" alt="happydemic">
              </div>
              <div class="item">
                  <img src="img/home/lupin.png" alt="happydemic">
                  <img src="img/home/ambuja.png" alt="happydemic">
              </div>
            </div>

            <div class="client_logo_slider owl-carousel owl-theme desktop_none" >
              <div class="item">
                <img src="img/home/sbi.png" alt="happydemic">
                <img src="img/home/fadring.png" alt="happydemic">
                <img src="img/home/citibank.png" alt="happydemic">
                <img src="img/home/sbi_general.png" alt="happydemic">
                <img src="img/home/kotak.png" alt="happydemic">
              </div>
              <div class="item">
                <img src="img/home/dbs.png" alt="happydemic">
                <img src="img/home/lupin.png" alt="happydemic">
                <img src="img/home/pg.png" alt="happydemic">
                <img src="img/home/kotak1.png" alt="happydemic">
                <img src="img/home/ambuja.png" alt="happydemic">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="section8 container">
    <div class="row">
      <div class="col">
        <div class="gry_text" data-aos="fade-up">OUR THINKING</div>
        <h2 data-aos="fade-up">Blogs and vlogs <a href="our-thinking.php" class="mobile_none">Go to Our Blogs <img src="img/home/right_arrow_blue.svg" alt="songstruck"> </a> </h2>
        <div  data-aos="fade-up">
          <div class="blog_slider owl-carousel owl-theme">
          <?php if(!empty($blogsResult)): ?>
            <?php foreach($blogsResult as $key=>$value): ?>
              
              <div class="card" data-aos="fade-up" data-aos-delay="800">
                <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                  <img src="<?= str_replace("D:/projects/happydemic/","",$value['blog_thumbnail'])?>" class="img-fluid" />
                  <a href="<?php echo ($key%2==0) ?  "blog_detail.php?blog=".$value['slug_field'] :  "blog-temp2.php?blog=".$value['slug_field']; ?>">
                    <div class="mask" ></div>
                  </a>
                </div>
                <div class="card-body">
                  <p class="card-text">
                    <small class="text-muted"><?=$value['created_at']?></small>
                  </p>
                  <h5 class="card-title"><?=$value['blog_heading']?> </h5>
                  <p class="card-text"><?=$value['blog_description']?></p>
                  <a href="<?php echo ($key%2==0) ?  "blog_detail.php?blog=".$value['slug_field'] :  "blog-temp2.php?blog=".$value['slug_field']; ?>" class="card-link">Read Article</a>
                </div>
              </div>
              <!-- <div class="card" data-aos="fade-up" data-aos-delay="900">
                <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                  <img src="img/home/vlogs2.png" class="img-fluid" />
                  <a href="#!">
                    <div class="mask" ></div>
                  </a>
                </div>
                <div class="card-body">
                  <p class="card-text">
                    <small class="text-muted">Jun 23, 2019</small>
                  </p>
                  <h5 class="card-title">Music - Touching Life, 360 Degree</h5>
                  <p class="card-text">The indomitable power to evoke emotions is a unique quality of music that has ...</p>
                  <a href="#" class="card-link">Read Article</a>
                </div>
              </div>
              <div class="card" data-aos="fade-up" data-aos-delay="1000">
                <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                  <img src="img/home/vlogs3.png" class="img-fluid" />
                  <a href="#!">
                    <div class="mask" ></div>
                  </a>
                </div>
                <div class="card-body">
                  <p class="card-text">
                    <small class="text-muted">Jun 23, 2019</small>
                  </p>
                  <h5 class="card-title">Music, The Universal Language!</h5>
                  <p class="card-text">A popular notion, music really does connect to everyone across the globe. Here ...</p>
                  <a href="#" class="card-link">Read Article</a>
                </div>
              </div> -->
            <?php endforeach; ?>
          <?php else: ?>
            <p>No Blogs</p>
          <?php endif; ?>
          </div>
        </div>
        <div class="desktop_none">
          <a href="our-thinking.php">Go to Our Blogs <img src="img/home/right_arrow_blue.svg" alt="songstruck"> </a>
        </div>
      </div>
    </div>
  </div>
  <div class="section9">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h2 data-aos="fade-up">Add Happydemic to my inbox</h2>
          <p data-aos="fade-up">We offer a distinctive voice to your orgainisation, relationships and talent.<br> Subscribe now!</p>
        </div>
        <div class="col-sm-6">
          <div class="form-outline" data-aos="fade-up">
            <i class="fas fa-envelope trailing"></i>
            <input type="text" id="form1" class="form-control form-icon-trailing" placeholder="Enter your email"/>
            <button class="btn btn-outline-primary" type="button" id="button-addon2" data-mdb-ripple-color="dark">
              Subscribe Here
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section10" id="contact_us">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="gry_text" data-aos="fade-up">GET IN TOUCH</div>
          <h2 data-aos="fade-up">Let’s connect</h2>
        </div>
        <div class="col-sm-6">
          <form action="/action_page.php">
            <div class="form-group" data-aos="fade-up">
              <label for="usr">NAME</label>
              <input type="text" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">CONTACT NUMBER</label>
              <input type="tel" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">EMAIL ID</label>
              <input type="email" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">NATURE OF ENQUIRY</label>
              <select class="form-control" name="">
                  <option value="">Corporate enquiries </option>
                  <option value="">Songstruck enquiries </option>
                  <option value="">Artist enquiries </option>
                  <option value="">Other enquiries </option>
              </select>
                <div class="arrow_down"></div>
            </div>
            <button data-aos="fade-up" type="button" class="btn btn-dark">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="section11">
    <div class="container">
      <div class="row">
        <div class="col">
          <h4 data-aos="fade-up"><span>Discover more about us</span> </h4>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="card" data-aos="fade-up" data-aos-delay="800">
            <img src="img/home/discover1.png" class="card-img" alt="..." />
            <a href="people.php" class="card-img-overlay">
              <h5 class="card-title">Our Team <img src="img/home/right_arrow.svg" alt="songstruck"></h5>
            </a>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="card " data-aos="fade-up" data-aos-delay="900">
            <img src="img/home/discover2.png" class="card-img" alt="..." />
            <a href="careers.php" class="card-img-overlay">
              <h5 class="card-title">Careers <img src="img/home/right_arrow.svg" alt="songstruck"></h5>
            </a>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="card " data-aos="fade-up" data-aos-delay="1000">
            <img src="img/home/discover3.png" class="card-img" alt="..." />
            <a href="our-thinking.php" class="card-img-overlay">
              <h5 class="card-title">Blogs & Vlogs <img src="img/home/right_arrow.svg" alt="songstruck"></h5>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">

    const audioPlayer = document.querySelector(".audio-player");
    const audio = new Audio(
      "audio/anthem.mp3"
    );
    //credit for song: Adrian kreativaweb@gmail.com
    audio.addEventListener(
      "loadeddata",
      () => {
        audioPlayer.querySelector(".time .length").textContent = getTimeCodeFromNum(
          audio.duration
        );
        audio.volume = .75;
      },
      false
    );

    //click on timeline to skip around
    const timeline = audioPlayer.querySelector(".timeline");
    timeline.addEventListener("click", e => {
      const timelineWidth = window.getComputedStyle(timeline).width;
      const timeToSeek = e.offsetX / parseInt(timelineWidth) * audio.duration;
      audio.currentTime = timeToSeek;
    }, false);

    //click volume slider to change volume
    const volumeSlider = audioPlayer.querySelector(".controls .volume-slider");

    //check audio percentage and update time accordingly
    setInterval(() => {
      const progressBar = audioPlayer.querySelector(".progress");
      progressBar.style.width = audio.currentTime / audio.duration * 100 + "%";
      audioPlayer.querySelector(".time .current").textContent = getTimeCodeFromNum(
        audio.currentTime
      );
    }, 500);

    //toggle between playing and pausing on button click
    const playBtn = audioPlayer.querySelector(".controls .toggle-play");
    playBtn.addEventListener(
      "click",
      () => {
        if (audio.paused) {
          playBtn.classList.remove("play");
          playBtn.classList.add("pause");
          $('.disc').addClass('play');
          audio.play();

        } else {
          playBtn.classList.remove("pause");
          playBtn.classList.add("play");
            $('.disc').removeClass('play');
          audio.pause();
        }
      },
      false
    );

    audioPlayer.querySelector(".volume-button").addEventListener("click", () => {
      const volumeEl = audioPlayer.querySelector(".volume-container .volume");
      audio.muted = !audio.muted;
      if (audio.muted) {
        volumeEl.classList.remove("fa-volume-up");
        volumeEl.classList.add("fa-volume-mute");
      } else {
        volumeEl.classList.add("fa-volume-up");
        volumeEl.classList.remove("fa-volume-mute");
      }
    });

    //turn 128 seconds into 2:08
    function getTimeCodeFromNum(num) {
      let seconds = parseInt(num);
      let minutes = parseInt(seconds / 60);
      seconds -= minutes * 60;
      const hours = parseInt(minutes / 60);
      minutes -= hours * 60;

      if (hours === 0) return `${minutes}:${String(seconds % 60).padStart(2, 0)}`;
      return `${String(hours).padStart(2, 0)}:${minutes}:${String(
        seconds % 60
      ).padStart(2, 0)}`;
    }


    $(document).ready(function() {
      $('#video_sec').hide();
      $('#play_youtube_btn').on('click', function() {
        if ($(this).attr('data-click') == 1) {
          $(this).attr('data-click', 0)
          $('#video_sec')[0].pause();
          $('#play_youtube_btn').removeClass("pause");
          $('#video_sec').hide();
          $('.video_play').removeClass("backgroundblack");
        } else {
          $(this).attr('data-click', 1)
          $('#video_sec')[0].play();
          $('#play_youtube_btn').addClass("pause");
          $('.video_play').addClass("backgroundblack");
          $('#video_sec').show();
        }
      });

      $('.home_banner').owlCarousel({
        items: 1,
        loop: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 8000,
        autoplaySpeed: 8000,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        nav: false
      });
      $('.slider_sec').owlCarousel({
        items: 1,
        loop: false,
        dots: false,
        autoplay: false,
        smartSpeed: 2000,
        animateOut: 'slideOutUp',
        animateIn: 'slideInUp',
        nav: false
      });

      $('.stories_slider').owlCarousel({
        center: true,
     items:3,
     loop:true,
     margin:10,
        dots: false,
        nav: true,
          stagePadding:200,
        navText: [
          "<img src='img/arrow_circle_left.svg'>",
          "<img src='img/arrow_circle_right.svg'>"
        ],
        responsive : {
            0 : {
            items: 1,
            nav:false,
            margin: 5,
            stagePadding:30,
            },
            768 : {
            items: 3,
            }
        }
      });

      $('.client_logo_slider').owlCarousel({
                items:5,
                loop:false,
                margin:30,
                nav:false,
                dots:false,
                autoplay: false,
                // slideTransition: 'linear',
                // autoplayTimeout: 6000,
                // autoplaySpeed: 6000,
                // autoplayHoverPause: true,
        responsive : {
            0 : {
            items: 2,
            nav: false,
            },
            768 : {
            items: 5,
            }
        }
      });
      $('.blog_slider').owlCarousel({
        stagePadding: 0,
        items: 3,
        loop: false,
        margin: 30,
        dots: false,
        center: false,
        autoplay: false,
        nav: false,
        responsive : {
            0 : {
            items: 1,
            center: true
            },
            768 : {
            items: 3,
            }
        }
      });

    });
  </script>

</body>

</html>
