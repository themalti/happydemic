<?php
require('config.php');

$errors = [];


$blogs = "CREATE TABLE blogs (
    id int(6) unsigned NOT NULL AUTO_INCREMENT,
    blog_heading varchar(255) NOT NULL,
    slug_field varchar(255) NOT NULL,
    blog_description longtext NOT NULL,
    first_content longtext NOT NULL,
    second_content varchar(255) NOT NULL,
    highlighted_content varchar(255) NOT NULL,
    third_content varchar(255) DEFAULT NULL,
    blog_tags varchar(255) NOT NULL,
    author_image varchar(255) DEFAULT NULL,
    author_name varchar(255) DEFAULT NULL,
    author_designation varchar(255) DEFAULT NULL,
    timetoread int(50) DEFAULT 10,
    is_published tinyint(1) NOT NULL DEFAULT 0,
    created_at datetime NOT NULL DEFAULT current_timestamp(),
    is_active tinyint(1) NOT NULL DEFAULT 1,
    deleted_at datetime DEFAULT NULL,
    PRIMARY KEY (id)
   ) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4";




$blog_images ="CREATE TABLE blog_images (
    id int(6) unsigned NOT NULL AUTO_INCREMENT,
    blog_id int(11) NOT NULL,
    blog_thumbnail varchar(255) NOT NULL,
    blog_banner varchar(255) NOT NULL,
    middle_image varchar(255) NOT NULL,
    PRIMARY KEY (id)
   ) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4";


$user_table = "CREATE TABLE users (
    id int(11) NOT NULL AUTO_INCREMENT,
    email_id varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    is_loggedIn tinyint(1) NOT NULL DEFAULT 0,
    created_at datetime NOT NULL DEFAULT current_timestamp(),
    deleted_at datetime DEFAULT NULL,
    PRIMARY KEY (id)
   ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4";


$tables = [$blogs, $blog_images, $user_table];


foreach($tables as $k => $sql){
    $query = $db->query($sql);

    if(!$query)
       $errors[] = "Table $k : Creation failed ($db->error)";
    else
       $errors[] = "Table $k : Creation done";
}


foreach($errors as $msg) {
   echo "$msg <br>";
}

$db->close();


?>