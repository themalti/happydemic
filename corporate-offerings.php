<?php include 'header.php';?>
  <!-- Start your project here-->
  <div class="section36 ">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h3 data-aos="fade-up">We specialise in new models of engagement
            by leveraging the disruptive <span class="color-yellow">power of music</span>
            to deliver solutions that inspire connection,
            change and commitment in your people -
            in meaningful and unexpected ways.</h3>
          <p data-aos="fade-up" class="mobile_none">By aligning with music experiences, organisations have an opportunity to create deep meaning and engagement with their employees and external stakeholders in moments when they feel the most alive and present.</p>
          <a data-aos="fade-up" href="contact-us.php" class="btn btn-dark mobile_none" data-mdb-ripple-color="dark">Get in touch <img src="img/solutions/white_arrow.svg" alt=""> </a>
        </div>
        <div class="col-sm-6">
          <img data-aos="zoom-up" src="img/solutions/banner.png" alt="">
          <p class="desktop_none" data-aos="fade-up">By aligning with music experiences, organisations have an opportunity to create deep meaning and engagement with their employees and external stakeholders in moments when they feel the most alive and present.</p>
          <a data-aos="fade-up" href="contact-us.php" class="btn btn-dark desktop_none" data-mdb-ripple-color="dark">Get in touch <img src="img/solutions/white_arrow.svg" alt=""> </a>
        </div>
      </div>
    </div>
  </div>

  <div class="section37">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 data-aos="fade-up">Our Capabilities</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-12">
          <div class="text_sec">
            <h4 data-aos="fade-up">For Brand Essence </h4>
            <h3 data-aos="fade-up">Music to enhance brand<br>
              marketing </h3>
            <p data-aos="fade-up">How do I ignite greater passion and engagement with
              my brand to drive growth?
            </p>
            <a data-aos="fade-up" href="brand-essence.php" class="btn btn-light" data-mdb-ripple-color="dark">Know more <img src="img/corporate/right_blue.svg" alt=""> </a>
          </div>
        </div>
        <div class="col-sm-6 col-12">
          <img data-aos="zoom-up" src="img/solutions/1.png" alt="" class="banner_img">
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-12">
          <img data-aos="zoom-up" src="img/solutions/2.png" alt="" class="banner_img">
        </div>
        <div class="col-sm-6 col-12">
          <div class="text_sec">
            <h4 data-aos="fade-up">For Revenue Impact</h4>
            <h3 data-aos="fade-up">Improve stakeholder <br>
              engagement</h3>
            <p data-aos="fade-up">How do I enthuse my trade and deepen our
              engagement with them for better business results?
            </p>
            <a data-aos="fade-up" href="revenue-impact.php" class="btn btn-light" data-mdb-ripple-color="dark">Know more <img src="img/corporate/right_blue.svg" alt=""> </a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-12">
          <div class="text_sec">
            <h4 data-aos="fade-up">For Organisation and Culture</h4>
            <h3 data-aos="fade-up">Music to build <br>
              culture and encourage <br>
              employee growth</h3>
            <p data-aos="fade-up">How do I use music to foster a culture of creativity
              and collaboration in my enterprise?
            </p>
            <a data-aos="fade-up" href="organisation-culture.php" class="btn btn-light" data-mdb-ripple-color="dark">Know more <img src="img/corporate/right_blue.svg" alt=""> </a>
          </div>
        </div>
        <div class="col-sm-6 col-12">
          <img data-aos="zoom-up" src="img/solutions/3.png" alt="" class="banner_img">
        </div>
      </div>
    </div>
  </div>

  <div class="section9">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h2 data-aos="fade-up">Add Happydemic to my inbox</h2>
          <p data-aos="fade-up">We offer a distinctive voice to your orgainisation, relationships and talent.<br> Subscribe now!</p>
        </div>
        <div class="col-sm-6">
          <div class="form-outline" data-aos="fade-up">
            <i class="fas fa-envelope trailing"></i>
            <input type="text" id="form1" class="form-control form-icon-trailing" />
            <button class="btn btn-outline-primary" type="button" id="button-addon2" data-mdb-ripple-color="dark">
              Subscribe Here
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End your project here-->
<?php include 'footer.php';?>
</body>

</html>
