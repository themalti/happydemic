<?php include 'header.php';?>
<?php include 'blog_detail_view.php'; ?>

  <!-- Start your project here-->
  <div class="section20">
    <div class="container">
      <div class="row">
        <div class="col">
          <h2 data-aos="fade-up"><?=$rowData['blog_heading']?></h2>
          <h4 data-aos="fade-up"><?=$rowData['author_name']?> &nbsp;| &nbsp;<?=$rowData['created_at']?></h4>
        </div>
      </div>
    </div>
  </div>
  <div class="section21 container">
    <div class="row">
      <div class="col">
        <div class="social_sec" data-aos="fade-up">
          <ul>
            <li>Share</li>
            <li> <a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a> </li>
            <li> <a href="#" target="_blank"><i class="fab fa-instagram"></i></a> </li>
            <li> <a href="#" target="_blank"><i class="fab fa-twitter"></i></a> </li>
            <li> <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a> </li>
          </ul>
        </div>
        <div class="main_sec">
          <p data-aos="fade-up"><?=$rowData['blog_description']?></p>
          <p data-aos="fade-up"><?=$rowData['first_content']?>
          </p>
          <div class="user_setail" data-aos="fade-up">
            <div class="user_img" data-aos="zoom-out">
              <img src="<?=str_replace("D:/projects/happydemic/","",$rowData['author_image'])?>" alt="">
            </div>
            <div class="user_detail_text">
              <h4><?=$rowData['author_name']?></h4>
              <h5><?=$rowData['author_designation']?></h5>
            </div>
            <div class="user_date">
              <p><?=$rowData['timetoread']?> minutes to read</p>
              <p><?=$rowData['created_at']?></p>
            </div>
          </div>
          <img src="<?=str_replace("D:/projects/happydemic/","",$rowData['middle_image'])?>" alt="" class="banner2">
          <p><?=$rowData['second_content']?></p>
          <!-- <h4 data-aos="fade-up">What is more important? Intelligence or Action? </h4>
          <p data-aos="fade-up">We are not saying that either of these traits are more important than the other, but if we were to examine what society would value more, the answer would unequivocally be intelligence. Most people you would meet would have an average
            IQ, only 2.2% of people in the world would have an IQ above average. </p>
          <p data-aos="fade-up">Furthermore, intelligence is something that can be linked to genetics, but determination is something that one has to develop by practicing over the years. Intelligence without the will to take action would be a terrible waste.</p>
          <p data-aos="fade-up">Since it has been established that the majority of people have average intelligence the only way to distinguish yourself from the rest is by taking action. Things such as IQ or EQ are measurable things that may be assigned to you, but
            there is no way that you should allow a number to dictate the actions you might take. </p>
          <p data-aos="fade-up">An average IQ is not an indicator of mediocrity, it is your AQ (action quotient) that will determine where you end up in life. For example, Muhaammad Ali had an IQ of 78, that never stopped him from being one of the greatest boxers.</p> -->
          <div class="yellow_back" data-aos="fade-up">
            <!-- <h3>An average IQ is not an indicator of mediocrity, <br>it is your AQ (action quotient) that will determine where you end up in life.</h3> -->
            <p><?=$rowData['highlighted_content']?></p>
          </div>
          <p><?=$rowData['third_content']?></p>
          <!-- <h4 data-aos="fade-up">So what exactly is Action Quotient?</h4>
          <p data-aos="fade-up">Even though it has the word ‘quotient’ attached to it, there is no real way of measuring it. It can be simply defined as your willingness to act and take control.
            Having a good IQ is a gift, but at the end of the day it is your willingness to persevere that will guide you. Come to think of it, action eats intelligence for breakfast. :) </p>
          <p data-aos="fade-up">Any good organisation looking to have a <a href="#">positive revenue impact</a> needs to embrace this diversity that both the thinkers and doers bring to the table. One cannot exist without the other. Though it may sound very divisive,
            it is a proven fact that the most effective organizations are the one that gets the thinkers to do and the doers to apply intelligence behind their actions. </p>
          <p data-aos="fade-up">At Happydemic, we have been engaging with organisations in catalyzing innovation, accelerating profitable business growth through a host of music-inspired solutions to help employees stay agile and motivated to deliver their best.
            You could speed-dial us <a href="#">here</a> or drop an email at <a href="#">inquiries@happydemic.com</a> to know more about our capabilities and how we can deploy the power of music to your organisation’s growth. </p> -->
          <div class="user_setail border_top" data-aos="fade-up">
            <div class="user_img">
              <img src="<?=str_replace("D:/projects/happydemic/","",$rowData['author_image'])?>" alt="">
            </div>
            <div class="user_detail_text">
              <h4><?=$rowData['author_name']?></h4>
              <h5><?=$rowData['author_designation']?></h5>
            </div>
            <div class="user_date">
              <p><?=$rowData['timetoread']?> minutes to read</p>
              <p><?=$rowData['created_at']?></p>
            </div>
          </div>
          <div class="tag_click" data-aos="fade-up">
            <ul>
              <?php foreach($blog_tags as $val): ?>
                <li> <a href="#"><?=$val?></a> </li>
              <!-- <li> <a href="#">Business</a> </li>
              <li> <a href="#">Engagement</a> </li> -->
              <?php endforeach; ?>
            </ul>
          </div>
          <div class="social_sec desktop_none" data-aos="fade-up">
            <ul>
              <li>Share</li>
              <li> <a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a> </li>
              <li> <a href="#" target="_blank"><i class="fab fa-instagram"></i></a> </li>
              <li> <a href="#" target="_blank"><i class="fab fa-twitter"></i></a> </li>
              <li> <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a> </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section22">
    <div class="container">
    <?php if(!empty($relatedblogsList)):?>
      <div class="row">
        <div class="col">
          <h2 data-aos="fade-up">Related articles</h2>
          <div class="articles owl-carousel owl-theme">
            <?php foreach($relatedblogsList as $k=>$value): ?>
            <div class="item">
              <div class="card">
                <img src="<?=str_replace("D:/projects/happydemic/","",$value['blog_thumbnail'])?>" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title"><?=$value['blog_heading']?></h5>
                  <!-- <p class="card-text">
                    <?=$value['blog_description']?>
                  </p> -->
                </div>
              </div>
            </div>
            <?php endforeach; ?>
            
            <!-- <div class="item">
              <div class="card">
                <img src="img/blog/article.png" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title">Fostering high productivity
                    amongst employees</h5>
                  <p class="card-text">
                    A popular notion, music really does connect to everyone across the globe. Here ...
                  </p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="card">
                <img src="img/blog/article.png" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title">Fostering high productivity
                    amongst employees</h5>
                  <p class="card-text">
                    A popular notion, music really does connect to everyone across the globe. Here ...
                  </p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="card">
                <img src="img/blog/article.png" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title">Fostering high productivity
                    amongst employees</h5>
                  <p class="card-text">
                    A popular notion, music really does connect to everyone across the globe. Here ...
                  </p>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    <?php endif;?> 
    </div>
  </div>

  <div class="section9">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h2 data-aos="fade-up">Add Happydemic to my inbox</h2>
          <p data-aos="fade-up">We offer a distinctive voice to your orgainisation, relationships and talent.<br> Subscribe now!</p>
        </div>
        <div class="col-sm-6">
          <div class="form-outline" data-aos="fade-up">
            <i class="fas fa-envelope trailing"></i>
            <input type="text" id="form1" class="form-control form-icon-trailing" placeholder="Enter your email"/>
            <button class="btn btn-outline-primary" type="button" id="button-addon2" data-mdb-ripple-color="dark">
              Subscribe Here
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.articles').owlCarousel({
        stagePadding: 0,
        items: 3,
        loop: false,
        margin: 30,
        dots: false,
        autoplay: false,
        smartSpeed: 2000,
        nav: true,
        navText: [
          "<img src='img/corporate/left_arrow.svg'>",
          "<img src='img/corporate/right_arrow.svg'>"
        ],
        responsive : {
            0 : {
            items: 1,
            nav: false,
            },
            768 : {
            items: 3,
            }
        }
      });
      });
  </script>
</body>

</html>
