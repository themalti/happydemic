<?php include 'header.php';?>
<?php 
  require('config.php');

  if ($db->connect_error) {
      die("Connection failed: " . $db->connect_error);
  }

  try{
      $recentResult = mysqli_query($db,"SELECT b.*,DATE_FORMAT(b.created_at, '%d %M %Y') as created_at, bi.blog_thumbnail, bi.blog_banner, bi.middle_image FROM blogs as b left join blog_images as bi on bi.blog_id=b.id where is_active=1 and is_published=1 order by id desc LIMIT 2");
      $recentBlogsResult = [];
      if (!empty($db->error)){
          throw new Exception();
      }
      while ($row = mysqli_fetch_assoc($recentResult)) {
          array_push($recentBlogsResult, $row);
      }
      
      
      $allResult = mysqli_query($db,"SELECT b.*,DATE_FORMAT(b.created_at, '%d %M %Y') as created_at, bi.blog_thumbnail, bi.blog_banner, bi.middle_image FROM blogs as b left join blog_images as bi on bi.blog_id=b.id where is_active=1 and is_published=1 order by id desc");
      if (!empty($db->error)){
          throw new Exception();
      }
      // var_dump($db->error);
      
      $blogsResult = [];

      while ($row = mysqli_fetch_assoc($allResult)) {
        array_push($blogsResult, $row);
      }

      


  }catch(PDOException $exception){
      var_dump($db->error,'dd');
      die('ERROR: ' . $exception->getMessage());
  }
?>
  <!-- Start your project here-->
  <div class="section17">
    <div class="container">
      <div class="row">
        <div class="col">
          <h4 data-aos="fade-up">Blogs & Vlogs </h4>
          <h2 data-aos="fade-up">Thought Leadership. Transformative Insights.<br> Tangible Outcomes.</h2>
        </div>
      </div>
    </div>
  </div>

  <div class="section18">
    <div class="container">
      <div class="row">
        <div class="col">
          <h2 data-aos="fade-up">Recently added articles</h2>
          <p data-aos="fade-up">Here’s what we’ve been upto recently</p>
        </div>
      </div>
      <?php foreach($recentResult as $key=>$value): ?>
      <div class="row main_blog">
        <div class="col-sm-6">
          <a  href="<?php echo ($key%2==0) ?  "blog_detail.php?blog=".$value['slug_field'] :  "blog-temp2.php?blog=".$value['slug_field']; ?>" class="card" data-aos="fade-up">
            <img src="<?= str_replace("D:/projects/happydemic/","",$value['blog_thumbnail'])?>" class="card-img" alt="..." />
          </a>
        </div>
        <div class="col-sm-6">
          <div class="text_sec">
              <a href="<?php echo ($key%2==0) ?  "blog_detail.php?blog=".$value['slug_field'] :  "blog-temp2.php?blog=".$value['slug_field']; ?>" class="card-title" data-aos="fade-up"><?=$value['blog_heading']?></a>
              <p class="card-text" data-aos="fade-up">
              <?=$value['blog_description']?> 
              </p>
              <div class="user_setail" data-aos="fade-up">
                <div class="user_img">
                  <img src="<?= str_replace("D:/projects/happydemic/","",$value['author_image'])?>" alt="">
                </div>
                <div class="user_detail_text">
                  <h4><?=$value['author_name']?> </h4>
                  <h5><?=$value['author_designation']?> </h5>
                </div>
                <div class="user_date">
                  <p><?=$value['created_at']?> </p>
                </div>
            </div>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
      <!-- <div class="row main_blog">
        <div class="col-sm-6">
          <a  href="blog_detail.php"  class="card" data-aos="fade-up">
            <img src="img/blog/thumb2.png" class="card-img" alt="..." />
          </a>
        </div>
        <div class="col-sm-6">
          <div class="text_sec">
            <a href="blog_detail.php" class="card-title" data-aos="fade-up">Coping with new normal through music</a>
            <p class="card-text" data-aos="fade-up">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
            </p>
            <div class="user_setail" data-aos="fade-up">
              <div class="user_img">
                <img src="img/blog/user.png" alt="">
              </div>
              <div class="user_detail_text">
                <h4>Amar Pandit</h4>
                <h5>Designation</h5>
              </div>
              <div class="user_date">
                <p>12 Aug 2020</p>
              </div>
            </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>

  <div class="section19 ">
    <?php if(!empty($allResult)):?>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2 data-aos="fade-up">All articles</h2>
          <p data-aos="fade-up">We set trends and understand our client’s requirements, here are some articles which people really loved</p>
        </div>
      </div>
      <div class="row">
        <?php foreach($allResult as $k=>$value): ?>
        <div class="col-sm-4">
          <a href="blog_detail.php?blog=<?=$value['slug_field']?>" class="card" data-aos="fade-up">
            <img src="<?=str_replace("D:/projects/happydemic/","",$value['blog_thumbnail'])?>" class="card-img" alt="..." />
            <div class="card-body">
              <h5 class="card-title"><?=$value['blog_heading']?></h5>
              <p class="card-text">
                <?=$value['blog_description'] ?>
              </p>
            </div>
          </a>
        </div>
        <?php endforeach; ?>
        <!-- <div class="col-sm-4">
          <a href="blog_detail.php" class="card" data-aos="fade-up">
            <img src="img/blog/article2.png" class="card-img" alt="..." />
            <div class="card-body">
              <h5 class="card-title">Fostering high productivity
                amongst employees</h5>
              <p class="card-text">
                A popular notion, music really does connect to everyone across the globe. Here ...
              </p>
            </div>
          </a>
        </div>
        <div class="col-sm-4">
          <a href="blog_detail.php" class="card" data-aos="fade-up">
            <img src="img/blog/article3.png" class="card-img" alt="..." />
            <div class="card-body">
              <h5 class="card-title">Fostering high productivity
                amongst employees</h5>
              <p class="card-text">
                A popular notion, music really does connect to everyone across the globe. Here ...
              </p>
            </div>
          </a>
        </div>
        <div class="col-sm-4">
          <a href="blog_detail.php" class="card" data-aos="fade-up">
            <img src="img/blog/article4.png" class="card-img" alt="..." />
            <div class="card-body">
              <h5 class="card-title">Fostering high productivity
                amongst employees</h5>
              <p class="card-text">
                A popular notion, music really does connect to everyone across the globe. Here ...
              </p>
            </div>
          </a>
        </div>
        <div class="col-sm-4">
          <a  href="blog_detail.php"class="card" data-aos="fade-up">
            <img src="img/blog/article5.png" class="card-img" alt="..." />
            <div class="card-body">
              <h5 class="card-title">Fostering high productivity
                amongst employees</h5>
              <p class="card-text">
                A popular notion, music really does connect to everyone across the globe. Here ...
              </p>
            </div>
          </a>
        </div>
        <div class="col-sm-4"> 
          <a href="blog_detail.php" class="card" data-aos="fade-up">
            <img src="img/blog/article6.png" class="card-img" alt="..." />
            <div class="card-body">
              <h5 class="card-title">Fostering high productivity
                amongst employees</h5>
              <p class="card-text">
                A popular notion, music really does connect to everyone across the globe. Here ...
              </p>
            </div>
          </a>
        </div>-->
      </div>
    </div>
    <?php else :?>
    <h2>NO Articles</h2>
    <?php endif; ?>
  </div>
  <div class="section9">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h2 data-aos="fade-up">Add Happydemic to my inbox</h2>
          <p data-aos="fade-up">We offer a distinctive voice to your orgainisation, relationships and talent. <br>Subscribe now!</p>
        </div>
        <div class="col-sm-6">
          <div class="form-outline" data-aos="fade-up">
            <i class="fas fa-envelope trailing"></i>
            <input type="text" id="form1" class="form-control form-icon-trailing" placeholder="Enter your email" />
            <button class="btn btn-outline-primary" type="button" id="button-addon2" data-mdb-ripple-color="dark">
              Subscribe Here
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.organisation').owlCarousel({
        stagePadding: 0,
        items: 1,
        loop: false,
        margin: 0,
        dots: false,
        autoplay: false,
        smartSpeed: 2000,
        nav: true,
        navText: [
          "<img src='img/corporate/left_arrow.svg'>",
          "<img src='img/corporate/right_arrow.svg'>"
        ],
      });
      });
      
  </script>
</body>

</html>
