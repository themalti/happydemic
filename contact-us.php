<?php include 'header.php';?>
  <!-- Start your project here-->
  <div class="section17_2">
    <div class="container">
      <div class="row">
        <div class="col">
          <h2 data-aos="fade-up">Contact Us</h2>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h4>Thank you for your interest in Happydemic. Collaboration and partnership are core to what we do and we look forward to hearing from you. Please use the form alongside to get in touch with us. Alternatively, you can call us on the numbers below </h4>
        </div>
      </div>
    </div>
  </div>

  <div class="section10 contact_us" id="contact_us">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 back_sec_div">
          <div class="background_sec">
            <h4>Contact Information</h4>
            <h5>Fill up the information and we’ll reach out to you.</h5>
              <ul>
                <li>
                  <p>Songstruck</p>
                  <a href="mailto:songstruck@happydemic.com">E: songstruck@happydemic.com</a>
                  <a href="tel:+91 81047 38049">T: +91 81047 38049</a>
                </li>
                <li>
                  <p>Artists</p>
                  <a href="mailto:samipajolly.shaw@happydemic.com">E:  samipajolly.shaw@happydemic.com</a>
                  <a href="tel:+91 97695 79110">T:  +91 97695 79110</a>
                </li>
                <li>
                  <p>Corporates</p>
                  <a href="mailto:sudha.shirodkar@happydemic.com">E: sudha.shirodkar@happydemic.com</a>
                  <a href="tel:+91 98199 03739">T: +91 98199 03739</a>
                </li>
                <li>
                  <p>Community</p>
                  <a href="mailto:info@happydemic.com">E: info@happydemic.com</a>
                  <a href="tel:+91 98199 03739">T: +91 98199 03739</a>
                </li>
                <li>
                  <p>Bespoke Solutions </p>
                  <a href="mailto:info@happydemic.com">E:  info@happydemic.com</a>
                  <a href="tel:+91 98199 03739">T:  +91 98199 03739</a>
                </li>
                <li>
                  <p>Other Inquiries</p>
                  <a href="mailto:info@happydemic.com">E: info@happydemic.com</a>
                  <a href="tel:+91 98199 03739">T: +91 98199 03739</a>
                </li>
            </ul>
        
          </div>

        </div>
        <div class="col-sm-5 text_sec">
          <h4>We’ll get in touch </h4>
          <p>Fill up the information and we’ll reach out to you.</p>
          <form action="/action_page.php">
            <div class="form-group" data-aos="fade-up">
              <label for="usr">NAME</label>
              <input type="text" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">CONTACT NUMBER</label>
              <input type="tel" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">EMAIL ID</label>
              <input type="email" class="form-control" >
            </div>
            <div class="form-group" data-aos="fade-up">
              <label for="pwd">NATURE OF ENQUIRY</label>
              <select class="form-control" name="">
                  <option value="">Corporate enquiries </option>
                  <option value="">Songstruck enquiries </option>
                  <option value="">Artist enquiries </option>
                  <option value="">Other enquiries </option>
              </select>
                <div class="arrow_down"></div>
            </div>
            <button data-aos="fade-up" type="button" class="btn btn-dark">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- End your project here-->
<?php include 'footer.php';?>

</body>

</html>
