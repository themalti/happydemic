<?php

    require('config.php');

    if ($db->connect_error) {
        die("Connection failed: " . $db->connect_error);
    }

    $stmt = $db->prepare("SELECT b.*,DATE_FORMAT(b.created_at, '%d %M %Y') as created_at,bi.blog_thumbnail, bi.blog_banner, bi.middle_image FROM blogs as b left join blog_images as bi on bi.blog_id=b.id where b.slug_field=?");
    $stmt->bind_param("s", $_GET["blog"] );
    $stmt->execute();
    $rowData = $stmt->get_result()->fetch_assoc();
    $blog_tags = ($rowData['blog_tags']) ? explode(",",$rowData['blog_tags']) : [];
    $stmt->close();

    $relatedblogQuery = "SELECT b.*,bi.blog_thumbnail FROM blogs as b left join blog_images as bi on bi.blog_id=b.id where (b.is_published=1 and b.is_active=1 and b.id NOT IN ( ". $rowData['id']."))";
    if(!empty($blog_tags)){
        $relatedblogQuery = $relatedblogQuery." and ( ";
        foreach ($blog_tags as $key => $value) {
            $relatedblogQuery = $relatedblogQuery ." b.blog_tags like '%".trim($value)."%'";
            if ($key+1 < count($blog_tags)){
                $relatedblogQuery = $relatedblogQuery . " OR ";
            }
        }
        $relatedblogQuery = $relatedblogQuery." ) ";
    }
 
    $relatedblogsList = [];
    $result = mysqli_query($db,$relatedblogQuery);
        
    
    if (!empty($db->error)){
        throw new Exception();
    }
    
    while ($row = mysqli_fetch_assoc($result)) {
           
        array_push($relatedblogsList, $row);
    }
    // var_dump($relatedblogsList);
    if (!empty($db->error)){
        throw new Exception();
    }

?>