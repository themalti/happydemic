<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>happydemic</title>
  <!-- MDB icon -->
  <link rel="icon" href="../img/home/favicon.png" type="image/x-icon" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" />
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" />
  <!-- MDB -->
  <link rel="stylesheet" href="../css/mdb.min.css" />
    <link rel="stylesheet" href="../css/magnific-popup.css">
   <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" href="../css/style.css" />
</head>
<?php
session_start();
if(!(isset($_SESSION['ID']))){
  header("Location: index.php");
}
?>
<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
        <a class="navbar-brand" href="index.php"><img src="../img/home/logo.svg" alt=""> </a>
        <div class="">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="logout.php">Log Out</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
<body>
  <div class="section61 container">
    <form id="blogForm" name="blogForm" method="POST" action="create.php" enctype="multipart/form-data">
    <div class="row">
      <div class="col">
        <div class="top_sec popupmodal">
            <a href="bloglist.php" class="back_arrow"><i class="fas fa-arrow-left"></i> Go Back</a>
            <!-- <a href="blogview.php" class="previewbtn"><i class="fas fa-eye"></i> Preview</a> -->
            <input type="submit" name="button" class="savechange formSubmitBtn" value="Save Changes" />
            <input type="submit" name="button" class="publish formSubmitBtn" value="Publish" />
            <a href="#delete" data-effect="mfp-zoom-in" class="deleteblog pop" >Delete</a>
        </div>
        <h2>Edit </h2>
      </div>
    </div>
    <div class="row padtop">
      <div class="col-3">
        <!-- Tab navs -->
      <div class="title_p">Sections</div>
        <div class="nav flex-column nav-tabs text-center" id="v-tabs-tab" role="tablist" aria-orientation="vertical">
          <a class="nav-link active" id="banner_tab" data-mdb-toggle="tab" href="#banner" role="tab" aria-controls="banner" aria-selected="true"><img src="img/dot.svg" alt=""> Blog contents</a>
          <a class="nav-link" id="content_tab" data-mdb-toggle="tab" href="#content" role="tab" aria-controls="content" aria-selected="false"><img src="img/dot.svg" alt="">Author and Tags</a>
        </div>
        <!-- Tab navs -->
      </div>

      <div class="col-9">
        <!-- Tab content -->
        <input type="hidden" value="" name="button" id="button_text" />
        <div class="tab-content" id="v-tabs-tabContent">
          <div class="tab-pane fade show active" id="banner" role="tabpanel" aria-labelledby="banner_tab">
            <div class="title_p">Home Page thumbnails Detail </div>
            <div class="form-group choosefile">
               <label for="blog_thumbnail" class="btn-1" id="blog_thumbnail_label" style="background-image: url(./img/thumb.png);"/></label>
               <input type="file" id="blog_thumbnail" name="blog_thumbnail" class="image_blogs" accept="image/*" >
               <div class="example">
                 <p>Example</p>
                 <img src="img/thumbex.png" alt="" id="thumbnail_preview">
               </div>
            </div>
            <div class="form-group">
              <label for="blog_heading">Banner/ Blog Heading</label>
              <input type="text" name="blog_heading" id="blog_heading" class="form-control">
            </div>
            <div class="form-group">
              <label for="blog_description">Blog Description <span>This text will be shown as a short description on Homepage</span></label>
              <textarea  class="form-control" rows="8" cols="80" name="blog_description" id="blog_description" ></textarea>
            </div>
            <div class="title_blog">Blog Page Detail </div>
            <div class="form-group choosefile">
               <label for="blog_banner" class="btn-1" id="blog_banner_label" style="background-image: url(./img/chooseimage.png);"/></label>
               <input type="file" id="blog_banner" name="blog_banner" accept="image/*" class="image_blogs">
            </div>
            <div class="form-group">
              <label for="first_content">Content<span>This text will appear right after the banner </span> </label>
              <textarea id="first_content" name="first_content" class="form-control" rows="15" cols="80"></textarea>
            </div>
            <div class="form-group choosefile">
               <label for="middle_image" class="btn-1" id="middle_image_label" style="background-image: url(./img/middleimage.png);"/></label>
               <input type="file" id="middle_image" name="middle_image" accept="image/*" class="image_blogs">
               <div class="example">
                 <p>Example</p>
                 <img src="img/middle.png" alt="">
               </div>
            </div>
            <div class="form-group">
              <label for="second_content">Content<span>This text will appear right after the middle banner </span> </label>
              <textarea id="second_content" name="second_content" class="form-control" rows="15" cols="80"></textarea>
            </div>
            <div class="form-group">
              <label for="highlighted_content">Highlighted Content<span>This content will appear in a yellow box</span> </label>
              <textarea id="highlighted_content" name="highlighted_content" class="form-control" rows="8" cols="80"></textarea>
              <div class="example">
                <p>Example</p>
                <img src="img/yellow.png" alt="">
              </div>
            </div>
            <div class="form-group">
              <label for="third_content">Content<span>This text will appear right after the middle banner </span> </label>
              <textarea name="third_content" id="third_content" class="form-control" rows="15" cols="80"></textarea>
            </div>
          </div>
          <div class="tab-pane fade" id="content" role="tabpanel" aria-labelledby="content_tab">
            <div class="author_sec">
              <p class="title">Author Detail</p>
                <div class="form-group ">
                  <label for="author_designation">Select Author</label>
                  <select class="form-control" name="">
                    <option value="">author name</option>
                  </select>
                </div>
                <div class="form-group ">
                  <label for="timetoread">Time to read  </label>
                  <input type="text" class="form-control" name="timetoread" id="timetoread">
                </div>
            </div>
            <div class="tags_sec">
                <div class="form-group">
                  <label for="usr">Add tags for this blog <span>Make sure you have relatable tags, this helps to find blogs easily</span> </label>
                  <input type="text" id="tag_text" class="form-control">
                  <input type="hidden" value="" name="blog_tags" id="blog_tags"/>
                  <button type="button" name="button" id="blog_tag_btn">Add</button>
                </div>
                <p class="title">Your tags</p>
                <div class="alerttags" id="your_tag_list"></div>
            </div>
          </div>
        </div>
        <!-- Tab content -->
      </div>
    </div>
    </form>
  </div>
  <!-- Popup itself -->
<div id="delete" class="mfp-with-anim mfp-hide popup_modal_size">
  <div class="modal_popup">
    <i class="fas fa-trash-alt"></i>
    <h2>Are you sure you want to delete this section?</h2>
    <div class="actionbtn">
      <button type="button" name="button" class="mfp-close">Cancel</button>
      <button type="button" name="button" >Delete Section</button>
    </div>
  </div>
</div>
<script type="text/javascript" src="../js/jquery3.3.1.min.js"></script>
<script type="text/javascript" src="../js/mdb.min.js"></script>
<script type="text/javascript" src="../js/jquery.magnific-popup.min.js"></script>
<script src="../js/jquery.validate.min.js"></script>
<script type="text/javascript">
$('.popupmodal').magnificPopup({
  delegate: 'a.pop',
  removalDelay: 500,
  callbacks: {
    beforeOpen: function() {
      this.st.mainClass = this.st.el.attr('data-effect');
    }
  },
  midClick: true
});

// Hinge effect popup
$('a.hinge').magnificPopup({
  mainClass: 'mfp-with-fade',
  removalDelay: 1000, //delay removal by X to allow out-animation
  callbacks: {
    beforeClose: function() {
      this.content.addClass('hinge');
    },
    close: function() {
      this.content.removeClass('hinge');
    }
  },
  midClick: true
});

$("#blog_tag_btn").click(function() {

    var tag = $("#tag_text").val();
    var html_str = '<div class="alert alert-dismissible fade show" role="alert" data-mdb-color="warning">'+tag+'<button type="button" class="btn-close" data-mdb-dismiss="alert" aria-label="Close"></button>';
    $("#your_tag_list").append(html_str);
    if($("#blog_tags").val() == ""){
      $("#blog_tags").val(tag);
    }else{
      var prev_val = $("#blog_tags").val();
      $("#blog_tags").val(prev_val +  ", " +tag);
    }
    $("#tag_text").val("");
});

function removeTag(tag_name){
    // alert(tag_name);
    var blogList = $('#blog_tags').val().split(',');
    console.log(blogList);
    var newList = jQuery.grep(blogList, function(value) {
          return value != tag_name;
        });

    $('#blog_tags').val(newList.toString());
  }
$("#blogForm").validate({
		// Specify validation rules
		rules: {
		// The key name on the left side is the name attribute
		// of an input field. Validation rules are defined
		// on the right side
		blog_thumbnail: "required",
		blog_banner: "required",
		middle_image: "required",
		blog_heading: "required",
		blog_description: "required",
		first_content: "required",
		second_content: "required",
		highlighted_content: "required",
		author_name: "required",
		author_designation: "required",
		author_image: "required",
		timetoread: "required",
    third_content: "required",
		// email: {
		// 	required: true,
		// 	// Specify that email should be validated
		// 	// by the built-in "email" rule
		// 	email: true
		// },
		// mobile:{
		// 	required: true,
		// 	minlength:10,
		// 	maxlength:10,
		// 	digits: true
		// }
		},
		// Specify validation error messages
		messages: {
    blog_heading: "Please enter a blog header",
		blog_description: "Please enter a blog description",
		// email: "Please enter a valid email address",
		// mobile: {
		// 		required: "Please enter your mobile number",
		// 		minlength: "Your mobile number must be at least 10 digits.",
		// 		maxlength: "Your mobile number must be maximum of 10 digits.",
		// 		digits: "Your mobile number must be of digits only."
		// 	}
		},
		// Make sure the form is submitted to the destination defined
		// in the "action" attribute of the form when valid
		submitHandler: function(form) {
      form.submit(function(){
        alert('Form submitted successfully.')
      });
		}
	});


$("#blogForm").submit(function(e) {

  e.preventDefault(); // avoid to execute the actual submit of the form.
  var form = $(this);
  console.log(e.originalEvent.submitter.value);
  // var form_data = new FormData();
  // if($("#blog_thumbnail")[0]['files'][0] ){
  //     alert("Please upload all images");
  //   }
  var form_data = {
    "blog_heading" : $("#blog_heading").val(),
    "blog_description" : $("#blog_description").val(),
    "first_content" : $("#first_content").val(),
    "second_content" : $("#second_content").val(),
    "highlighted_content" : $("#highlighted_content").val(),
    "third_content" : $("#third_content").val(),
    "blog_tags" : $("#blog_tags").val(),
    "is_published" : e.originalEvent.submitter.value,
    "author_name" : $("#author_name").val(),
    "author_designation" : $("#author_designation").val(),
    "timetoread" : $("#timetoread").val(),
    "author_image" : $("#author_image")[0]['files'][0],
    "blog_thumbnail" : $("#blog_thumbnail")[0]['files'][0],
    "blog_banner" : $("#blog_banner")[0]['files'][0],
    "middle_image" : $("#middle_image")[0]['files'][0],
  };
  console.log(form_data);
  console.log($("#blogForm").valid());
  var url = form.attr('action');
  // var data = form.serialize();
  if(($("#blogForm").valid())){

    $.ajax({
      type: "POST",
      url: url,
      data: form_data ,
      dataType: 'json',
      contentType: "application/json; charset=utf-8",
      enctype: 'multipart/form-data',
      cache: false,
      processData: false,
      success: function(data)
      {
        alert("Form Submitted Successfully.");
        // $("#form_alert").show();
        // setTimeout(function(){ $("#form_alert").css("display","block"); }, 8000);
        // location.reload();
        // alert("Form Submitted Successfully."); // show response from the php script.
      }
    });
  }
  // else{
  //   alert("Please upload all images");
  //   e.preventDefault();
  // }
});

$(".formSubmitBtn").click(function(e){
  console.log($("#blogForm").valid());
  if($("#blogForm").valid()){
        e.preventDefault();
    // alert('fkj');

    $("#content").addClass("active show");
    $("#content_tab").addClass("active show");
    $("#banner").removeClass("active show");
    $("#banner_tab").removeClass("active");

    if($("#blogForm").valid() && ($("#author_name").val() != "")){
      $("#button_text").val($(this).val());
      $("#blogForm").submit();
    }

  }
})

// $("#blog_thumbnail").change(function(){
//   $("#thumbnail_preview").attr('src',"//"+$(this).val());
// });


$(".image_blogs").on("change", function(){
    var idText = $(this)[0].id + "_label";
    var file = $(this).get(0).files[0];
    if(file){
        var reader = new FileReader();

        reader.onload = function(){
            $("#"+idText).css("background-image", "url('"+reader.result+"')");
        }

        reader.readAsDataURL(file);
    }

});

</script>
</body>

</html>
