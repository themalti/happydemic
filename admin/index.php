<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>happydemic</title>
  <!-- MDB icon -->
  <link rel="icon" href="../img/home/favicon.png" type="image/x-icon" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" />
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" />
  <!-- MDB -->
  <link rel="stylesheet" href="../css/mdb.min.css" />
   <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" href="../css/style.css" />
</head>
<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
        <a class="navbar-brand" href="index.php"><img src="../img/home/logo.svg" alt=""> </a>
      </div>
    </nav>
  </header>
<body>
<section class="section59">
  <div class="loginsec">
    <form class="" action="admin-login.php" method="post">
      <h3>Admin Login</h3>
      <div class="form-group">
          <label for="usr">Username <span>*</span> </label>
          <input type="email" name="email_id" id="email_id" class="form-control form-field" required>
        </div>
        <div class="form-group">
          <label for="usr">Password <span>*</span> </label>
          <input type="password" class="form-control form-field" name="password" id="password" required>
        </div>
        <button type="submit" name="button">Login</button>
    </form>
    <?php session_start(); if(isset($_SESSION['login_error'])) echo "<div id='togglediv'>".$_SESSION["login_error"]." </div>"; ?>
  </div>

</section>

<script type="text/javascript" src="../js/jquery3.3.1.min.js"></script>
<script type="text/javascript" src="../js/mdb.min.js"></script>
<script type="text/javascript">
    <?php if(isset($_SESSION['login_error'])){ ?>
      $(".form-field").css("border", "1px solid #f93154");
    <?php } ?>
</script>
</body>



</html>
