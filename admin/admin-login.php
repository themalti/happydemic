<?php
require('../config.php');

ob_start();
session_start();


if (isset($_POST['email_id']) && !empty($_POST['email_id']) && !empty($_POST['password'])) {
				
    // Check connection
    if ($db->connect_error) {
        die("Connection failed: " . $db->connect_error);
    }
    
    // prepare and bind
    $email = $_POST['email_id'];
    $pass = md5($_POST['password']);
    $stmt = $db->prepare("SELECT id FROM users where email_id=? and password =?");
    
    $stmt->bind_param("ss", $email, $pass );
    $stmt->execute();
    $row = $stmt->get_result()->fetch_assoc();
    // var_dump($row);
    $stmt->close();

    if(is_array($row))
    {
        unset($_SESSION["login_error"]);
        $_SESSION["ID"] = $row['id'];
        // $_SESSION["Email"]=$row['Email'];
        // $_SESSION["First_Name"]=$row['First_Name'];
        // $_SESSION["Last_Name"]=$row['Last_Name']; 
        header("Location: bloglist.php"); 
    }
    else
    {
        $_SESSION["login_error"] = "Email Id and password does not match.";
        header("Location: index.php"); 
    }
}

?> 