<?php 

    // include database connection
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    if(!(isset($_SESSION['ID']))){
        header("Location: index.php"); 
      }

    require('../config.php');

    if ($db->connect_error) {
        die("Connection failed: " . $db->connect_error);
    }

    try{
        $result = mysqli_query($db,"SELECT b.*,DATE_FORMAT(b.created_at, '%d %M %Y') as created_at,bi.blog_thumbnail, bi.blog_banner, bi.middle_image FROM blogs as b left join blog_images as bi on bi.blog_id=b.id order by id desc");
        
       
        if (!empty($db->error)){
            throw new Exception();
        }
        
        $blogsResult = [];

        while ($row = mysqli_fetch_assoc($result)) {
           
            array_push($blogsResult, $row);
        }


    }catch(PDOException $exception){
        var_dump($db->error,'dd');
        die('ERROR: ' . $exception->getMessage());
    }
?>