<?php

define ('SITE_ROOT', realpath(dirname(__FILE__)));
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if(!(isset($_SESSION['ID']))){
  header("Location: index.php");
}
if($_POST){

    try{

        // include database connection
        require('../config.php');

         // posted values
         $blog_heading=$_POST['blog_heading'];
         $blog_description=$_POST['blog_description'];
         $first_content=$_POST['first_content'];
         $highlighted_content=$_POST['highlighted_content'];
         $third_content=$_POST['third_content'];
         $blog_tags= $_POST['blog_tags'];
         // $author_image=$_POST['author_image'];
         $author_name=$_POST['author_name'];
         $author_designation=$_POST['author_designation'];
         $timetoread=$_POST['timetoread'];
         $is_published=$_POST['button']  == "Publish" ? 1 : 0;
         $second_content = $_POST['second_content'];

         // Images
         $imageArray = [];

        // var_dump($_POST);

        if(isset($_POST['blog_id'])){
            $id = $_POST['blog_id'];
            // $getBlogQuery =

            $query = " UPDATE blogs set blog_heading=?, blog_description=?, first_content=?,second_content=?, highlighted_content=?,third_content=?,
            blog_tags=?, author_name=?, author_designation=?, timetoread=?, is_published=? where id = ? ";

            $stmt = $db->prepare($query);

            $stmt->bind_param('sssssssssiii', $blog_heading, $blog_description, $first_content, $second_content, $highlighted_content,
                $third_content, $blog_tags, $author_name, $author_designation, $timetoread, $is_published, $id
            );

            if($stmt->execute()){
                $targetDir = $_SERVER['DOCUMENT_ROOT']."/admin/BlogImages/".$id."/";

                foreach($_FILES as $key=>$val){
                    // File upload path
                    if($val['error'] != 4){
                        $fileName = basename($val['name']);
                        $targetFilePath = $targetDir. $fileName;
                        $d = move_uploaded_file($val['tmp_name'],$targetFilePath);

                        if($d){
                            if ($key == "author_image"){
                                $updateQuery="UPDATE blogs SET author_image='".$targetFilePath."' WHERE id=$id";
                                mysqli_query($db,$updateQuery);
                            }else{
                                $imagequery = "UPDATE blog_images SET ".$key."='".$targetFilePath."' WHERE blog_id=$id";

                                mysqli_query($db,$imagequery);
                            }
                            // $imageArray[$key] = $targetFilePath;
                        }else{
                            $imageArray[$key] = "";
                        }

                    }

                }

                $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', strtolower($blog_heading));
                $query = "SELECT slug_field FROM blogs WHERE slug_field LIKE '$slug%'";

                $statement = $db->prepare($query);
                if($statement->execute())
                {
                    // $total_row = $statement->get_result();
                    $resultSet = $statement->get_result();
                    $data = $resultSet->fetch_all(MYSQLI_ASSOC);
                    if(count($data) > 0)
                    {
                        // $result = $statement->fetch_all();
                        foreach($data as $row)
                        {
                            $data[] = $row['slug_field'];
                        }

                        if(in_array($slug, $data))
                        {
                            $count = 0;
                            while( in_array( ($slug . '-' . ++$count ), $data) );
                            $slug = $slug . '-' . $count;
                        }
                    }
                }
                $updateSlugQuery="UPDATE blogs SET slug_field='".$slug."' WHERE id=$id";
                mysqli_query($db,$updateSlugQuery);
                var_dump($db->error);
                var_dump($slug,$id);

                // if (array_key_exists( "author_image", $imageArray) ){
                //     $updateQuery="UPDATE blogs SET author_image='".$imageArray["author_image"]."' WHERE id=$id";
                //     mysqli_query($db,$updateQuery);
                // }

                // $imagequery = "UPDATE blog_images set blog_id=?, blog_thumbnail, blog_banner, middle_image) values
                //         ($id, '".$imageArray['blog_thumbnail']."', '".$imageArray['blog_banner']."', '".$imageArray['middle_image']."')  ";

                // mysqli_query($db,$imagequery);
            }else{
                var_dump($db->error);
                // echo "<div class='alert alert-danger'>Unable to save record.</div>";
            }

        }else{
            //Blog Creation
            // insert query

            $query = "INSERT INTO blogs (blog_heading, blog_description, first_content, second_content, highlighted_content,third_content,
            blog_tags, author_name, author_designation, timetoread, is_published ) values
            (?,?,?,?,?,?,?,?,?,?,?)  ";

            // SET blog_heading=:blog_heading, blog_description=:blog_description, first_content=:first_content, highlighted_content=:highlighted_content, third_content=:third_content,
            //         blog_tags=:blog_tags,author_image=:author_image, author_name=:author_name, author_designation=:author_designation,
            //         timetoread=:timetoread, is_published=:is_published

            // prepare query for execution
            $stmt = $db->prepare($query);



            // is_published

            // bind the parameters
            // $stmt->bind_param(':blog_heading', $blog_heading);
            // $stmt->bind_param(':blog_description', $blog_description);
            // $stmt->bind_param(':first_content', $first_content);
            // $stmt->bind_param(':highlighted_content', $highlighted_content);
            // $stmt->bind_param(':third_content', $third_content);
            // $stmt->bind_param(':blog_tags', $blog_tags);
            // $stmt->bind_param(':author_image', $author_image);
            // $stmt->bind_param(':author_name', $author_name);
            // $stmt->bind_param(':author_designation', $author_designation);
            // $stmt->bind_param(':is_published', $is_published);

            $stmt->bind_param('sssssssssii', $blog_heading, $blog_description, $first_content, $second_content, $highlighted_content,
                $third_content, $blog_tags, $author_name, $author_designation, $timetoread, $is_published
            );


            // specify when this record was inserted to the database
            // $created=date('Y-m-d H:i:s');
            // $stmt->bindParam(':created', $created);

            // Execute the query

            if($stmt->execute()){

                $last_id =$db->insert_id;
                $targetDir = $_SERVER['DOCUMENT_ROOT']."/admin/BlogImages/".$last_id."/";

                # create directory if not exists in upload/ directory
                if(!is_dir($targetDir)){
                    mkdir($targetDir, 0755);
                }

                foreach($_FILES as $key=>$val){
                    // File upload path
                    $fileName = basename($val['name']);
                    $targetFilePath = $targetDir. $fileName;
                    $d = move_uploaded_file($val['tmp_name'],$targetFilePath);

                    if($d){
                        $imageArray[$key] = $targetFilePath;
                    }else{
                        $imageArray[$key] = "";
                    }

                }

                $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $blog_heading);
                $query = "SELECT slug_field FROM blogs WHERE slug_field LIKE '$slug%'";

                $statement = $db->prepare($query);
                if($statement->execute())
                {
                    // $total_row = $statement->get_result();
                    $resultSet = $statement->get_result();
                    $data = $resultSet->fetch_all(MYSQLI_ASSOC);
                    if(count($data) > 0)
                    {
                        // $result = $statement->fetch_all();
                        foreach($data as $row)
                        {
                            $data[] = $row['slug_field'];
                        }

                        if(in_array($slug, $data))
                        {
                            $count = 0;
                            while( in_array( ($slug . '-' . ++$count ), $data) );
                            $slug = $slug . '-' . $count;
                        }
                    }
                }
                $updateSlugQuery="UPDATE blogs SET slug_field='".$slug."' WHERE id=$last_id";
                mysqli_query($db,$updateSlugQuery);

                if (array_key_exists( "author_image", $imageArray) ){
                    $updateQuery="UPDATE blogs SET author_image='".$imageArray["author_image"]."' WHERE id=$last_id";
                    mysqli_query($db,$updateQuery);
                }



                $imagequery = "INSERT INTO blog_images (blog_id, blog_thumbnail, blog_banner, middle_image) values
                        ($last_id, '".$imageArray['blog_thumbnail']."', '".$imageArray['blog_banner']."', '".$imageArray['middle_image']."')  ";

                mysqli_query($db,$imagequery);

                echo "<div class='alert alert-success'>Record was saved.</div>";
            }else{
                var_dump($db->error);
                // echo "<div class='alert alert-danger'>Unable to save record.</div>";
            }
        }

        header("Location: bloglist.php"); 
    }

    // show error
    catch(PDOException $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}


?>
