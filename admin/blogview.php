<?php

require('../config.php');

if ($db->connect_error) {
    die("Connection failed: " . $db->connect_error);
}

$stmt = $db->prepare("SELECT b.*,DATE_FORMAT(b.created_at, '%d %M %Y') as created_at,bi.blog_thumbnail, bi.blog_banner, bi.middle_image FROM blogs as b left join blog_images as bi on bi.blog_id=b.id where b.slug_field=?");
    $stmt->bind_param("s", $_GET["blog"] );
    $stmt->execute();
    $rowData = $stmt->get_result()->fetch_assoc();
    $blog_tags = ($rowData['blog_tags']) ? explode(",",$rowData['blog_tags']) : [];
    $stmt->close();

    $relatedblogQuery = "SELECT b.*,bi.blog_thumbnail FROM blogs as b left join blog_images as bi on bi.blog_id=b.id where (b.is_published=1 and b.is_active=1 and b.id NOT IN ( ". $rowData["id"]."))";
    if(!empty($blog_tags)){
        $relatedblogQuery = $relatedblogQuery." and ( ";
        foreach ($blog_tags as $key => $value) {
            $relatedblogQuery = $relatedblogQuery ." b.blog_tags like '%".trim($value)."%'";
            if ($key+1 < count($blog_tags)){
                $relatedblogQuery = $relatedblogQuery . " OR ";
            }
        }
        $relatedblogQuery = $relatedblogQuery." ) ";
    }

    $relatedblogsList = [];
    $result = mysqli_query($db,$relatedblogQuery);


    if (!empty($db->error)){
        throw new Exception();
    }

    while ($row = mysqli_fetch_assoc($result)) {

        array_push($relatedblogsList, $row);
    }
    // var_dump($relatedblogsList);
    if (!empty($db->error)){
        throw new Exception();
    }
?>
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>happydemic</title>
  <!-- MDB icon -->
<link rel="icon" href="../img/home/favicon.png" type="image/x-icon" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" />
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" />
  <!-- MDB -->
  <link rel="stylesheet" href="../css/owl.carousel.min.css">
  <link rel="stylesheet" href="../css/owl.theme.default.min.css">
  <link rel="stylesheet" href="../css/magnific-popup.css">
  <link rel="stylesheet" href="../css/mdb.min.css" />
   <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" href="../css/style.css" />
</head>
<?php
session_start();
if(!(isset($_SESSION['ID']))){
  header("Location: index.php");
}
?>
<header>
  <nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
      <a class="navbar-brand" href="index.php"><img src="../img/home/logo.svg" alt=""> </a>
      <div class="">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="logout.php">Log Out</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<body>

  <!-- Start your project here-->
  <div class="section61 container backbtn">
    <div class="row">
      <div class="col">
        <div class="top_sec popupmodal">
          <a href="bloglist.php" class="back_arrow"><i class="fas fa-arrow-left"></i> Go Back</a>
        </div>
      </div>
    </div>
  </div>
  <div class="section20" style="background-image:url(<?= str_replace("D:/projects/happydemic/admin/","",$rowData['blog_thumbnail'])?>)">
    <div class="container">
      <div class="row">
        <div class="col">
          <h2><?=$rowData['blog_heading']?></h2>
          <h4><?=$rowData['author_name']?> &nbsp;| &nbsp;<?=$rowData['created_at']?></h4>
        </div>
      </div>
    </div>
  </div>
  <div class="section21 container">
    <div class="row">
      <div class="col">
        <div class="social_sec">
          <ul>
            <li>Share</li>
            <li> <a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a> </li>
            <li> <a href="#" target="_blank"><i class="fab fa-instagram"></i></a> </li>
            <li> <a href="#" target="_blank"><i class="fab fa-twitter"></i></a> </li>
            <li> <a href="#" target="_blank"><i class="fab fa-linkedin-in"></i></a> </li>
          </ul>
        </div>
        <div class="main_sec">
          <p data-aos="fade-up"><?=$rowData['blog_description']?></p>
          <p data-aos="fade-up"><?=$rowData['first_content']?>
          <div class="user_setail">
            <div class="user_img" data-aos="zoom-out">
              <img src="<?= str_replace("D:/projects/happydemic/admin/","",$rowData['author_image'])?>" alt="">
            </div>
            <div class="user_detail_text">
              <h4><?=$rowData['author_name']?></h4>
              <h5><?=$rowData['author_designation']?></h5>
            </div>
            <div class="user_date">
              <p><?=$rowData['timetoread']?> minutes to read</p>
              <p><?=$rowData['created_at']?></p>
            </div>
          </div>
          <img src="<?= str_replace("D:/projects/happydemic/admin/","",$rowData['blog_banner'])?>" alt="" class="banner2">
          <!-- <h4>What is more important? Intelligence or Action? </h4> -->
          <p><?=$rowData['second_content']?></p>
          <div class="yellow_back">
            <!-- <h3>An average IQ is not an indicator of mediocrity, <br>it is your AQ (action quotient) that will determine where you end up in life.</h3> -->
            <h3><?=$rowData['highlighted_content']?></h3>
          </div>
          <p><?=$rowData['third_content']?></p>
          <!-- <h4>So what exactly is Action Quotient?</h4>
          <p>Even though it has the word ‘quotient’ attached to it, there is no real way of measuring it. It can be simply defined as your willingness to act and take control.
            Having a good IQ is a gift, but at the end of the day it is your willingness to persevere that will guide you. Come to think of it, action eats intelligence for breakfast. :) </p>
          <p>Any good organisation looking to have a <a href="#">positive revenue impact</a> needs to embrace this diversity that both the thinkers and doers bring to the table. One cannot exist without the other. Though it may sound very divisive,
            it is a proven fact that the most effective organizations are the one that gets the thinkers to do and the doers to apply intelligence behind their actions. </p> -->
          <!-- <p>At Happydemic, we have been engaging with organisations in catalyzing innovation, accelerating profitable business growth through a host of music-inspired solutions to help employees stay agile and motivated to deliver their best.
            You could speed-dial us <a href="#">here</a> or drop an email at <a href="#">inquiries@happydemic.com</a> to know more about our capabilities and how we can deploy the power of music to your organisation’s growth. </p> -->
          <div class="user_setail border_top">
            <div class="user_img">
              <img src="<?= str_replace("D:/projects/happydemic/admin/","",$rowData['author_image'])?>" alt="">
            </div>
            <div class="user_detail_text">
              <h4><?=$rowData['author_name']?></h4>
              <h5><?=$rowData['author_designation']?></h5>
            </div>
            <div class="user_date">
              <p><?=$rowData['timetoread']?> minutes to read</p>
              <p><?=$rowData['created_at']?></p>
            </div>
          </div>
          <div class="tag_click" data-aos="fade-up">
            <ul>
              <?php foreach($blog_tags as $val): ?>
                <li> <a href="#"><?=$val?></a> </li>
              <!-- <li> <a href="#">Business</a> </li>
              <li> <a href="#">Engagement</a> </li> -->
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section22">
    <div class="container">
    <?php if(!empty($relatedblogsList)):?>
      <div class="row">
        <div class="col">
          <h2 data-aos="fade-up">Related articles</h2>
          <div class="articles owl-carousel owl-theme">
            <?php foreach($relatedblogsList as $k=>$value): ?>
            <div class="item">
              <div class="card">
                <img src="<?=str_replace("D:/projects/happydemic/admin/","",$value['blog_thumbnail'])?>" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title"><?=$value['blog_heading']?></h5>
                  <!-- <p class="card-text">
                    <?=$value['blog_description']?>
                  </p> -->
                </div>
              </div>
            </div>
            <?php endforeach; ?>

            <!-- <div class="item">
              <div class="card">
                <img src="img/blog/article.png" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title">Fostering high productivity
                    amongst employees</h5>
                  <p class="card-text">
                    A popular notion, music really does connect to everyone across the globe. Here ...
                  </p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="card">
                <img src="img/blog/article.png" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title">Fostering high productivity
                    amongst employees</h5>
                  <p class="card-text">
                    A popular notion, music really does connect to everyone across the globe. Here ...
                  </p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="card">
                <img src="img/blog/article.png" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title">Fostering high productivity
                    amongst employees</h5>
                  <p class="card-text">
                    A popular notion, music really does connect to everyone across the globe. Here ...
                  </p>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    <?php endif;?>
    </div>
  </div>


</body>
  <!-- End your project here-->
  <script type="text/javascript" src="../js/jquery3.3.1.min.js"></script>
  <script type="text/javascript" src="../js/mdb.min.js"></script>
  <script src="../js/owl.carousel.min.js"></script>
  <script src="../js/owl.carousel2.thumbs.js"></script>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.articles').owlCarousel({
        stagePadding: 0,
        items: 3,
        loop: false,
        margin: 30,
        dots: false,
        autoplay: false,
        smartSpeed: 2000,
        nav: true,
        navText: [
          "<img src='img/left_arrow.svg'>",
          "<img src='img/right_arrow.svg'>"
        ],
      });
    });

  </script>
</body>

</html>
