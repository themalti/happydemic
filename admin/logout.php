<?php
   session_start();
   unset($_SESSION["ID"]);
   unset($_SESSION["login_error"]);
   header('Refresh: 1; URL = index.php');
?>