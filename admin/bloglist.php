<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title>happydemic</title>
  <!-- MDB icon -->
  <link rel="icon" href="../img/home/favicon.png" type="image/x-icon" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" />
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" />
  <!-- MDB -->
  <link rel="stylesheet" href="../css/mdb.min.css" />
    <link rel="stylesheet" href="../css/magnific-popup.css">
   <link rel="stylesheet" type="text/css" href="../css/animate.css">
  <link rel="stylesheet" href="../css/style.css" />
</head>
<?php
session_start();
if(!(isset($_SESSION['ID']))){
  header("Location: index.php");
}
?>
<?php

include 'list.php';

?>
<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="container">
        <a class="navbar-brand" href="index.php"><img src="../img/home/logo.svg" alt=""> </a>
        <div class="">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link" href="logout.php">Log Out</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
<body>

  <div class="section60 container backend">
    <div class="row">
      <div class="col">
        <div class="nav flex-column nav-tabs text-center" id="v-tabs-tab" role="tablist" aria-orientation="vertical">
          <a class="nav-link active" id="banner_tab" data-mdb-toggle="tab" href="#banner" role="tab" aria-controls="banner" aria-selected="true">Blogs & Vlogs</a>
          <a class="nav-link" id="content_tab" data-mdb-toggle="tab" href="#content" role="tab" aria-controls="content" aria-selected="false">Authors</a>
        </div>

        <div class="edit_sec">
          <a href="blog-create.php" type="button" name="button"><i class="fas fa-plus"></i> Create Blogs</a>
          <a href="blog-create.php" type="button" name="button"><i class="fas fa-plus"></i> Create Authors</a>
        </div>
      </div>

    </div>



      <div class="tab-content" id="v-tabs-tabContent">
        <div class="tab-pane fade show active" id="banner" role="tabpanel" aria-labelledby="banner_tab">
          <div class="row">
          <?php if(!empty($blogsResult)): ?>
            <?php foreach($blogsResult as $key=>$value): ?>
              <div class="col-4">
                <div class="card" data-aos="fade-up" data-aos-delay="800">
                  <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="<?= str_replace("D:/projects/happydemic/admin/","",$value['blog_thumbnail'])?>" class="img-fluid" />
                    <div class="card-img-overlay popupmodal">
                      <a href="blogedit.php?blog=<?=$value['slug_field']?>" type="button" name="button" class="edit">Edit</a>
                      <a href="blogview.php?blog=<?=$value['slug_field']?>" type="button" name="button" class="view">view</a>
                      <a href="#delete" data-effect="mfp-zoom-in"  type="button" data="<?=$value['id']?>" name="button" class="delete pop delete_blog">delete</a>
                    </div>
                  </div>
                  <div class="card-body">
                    <p class="card-text">
                      <small class="text-muted"><?=$value['created_at']?></small>
                    </p>
                    <h5 class="card-title"><?=$value['blog_heading']?></h5>
                    <p class="card-text"><?=$value['blog_description']?></p>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          <?php else: ?>
            <p>No Blogs</p>
          <?php endif; ?>
          </div>
        </div>
        <div class="tab-pane fade" id="content" role="tabpanel" aria-labelledby="content_tab">
        <div class="row">
          <div class="col-4">
            <div class="user_setail popupmodal" data-aos="fade-up">
              <div class="user_img">
                <img src="img/author.png" alt="">
              </div>
              <div class="user_detail_text">
                <h4>Author name</h4>
                <h5>co founder And ceo</h5>
              </div>
              <div class="btn_sec">
                <a href="" type="button" name="button" class="edit">Edit</a>
                <a href="#delete" data-effect="mfp-zoom-in"  type="button" data="<?=$value['id']?>" name="button" class="delete pop delete_blog">delete</a>
              </div>
            </div>
          </div>
          <div class="col-4">
            <div class="user_setail popupmodal" data-aos="fade-up">
              <div class="user_img">
                <img src="img/author.png" alt="">
              </div>
              <div class="user_detail_text">
                <h4>Author name</h4>
                <h5>co founder And ceo</h5>
              </div>
              <div class="btn_sec">
                <a href="" type="button" name="button" class="edit">Edit</a>
                <a href="#delete" data-effect="mfp-zoom-in"  type="button" data="<?=$value['id']?>" name="button" class="delete pop delete_blog">delete</a>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-5">
            <div class="author_sec">
              <div class="left">
                <label for="author_image" class="btn-1" id="author_image_label" style="background-image: url('./img/author.png');"/></label>
                  <input type="file"  name="author_image" id="author_image" accept="image/*" class="image_blogs">
                  <p>Add Author image</p>
              </div>
              <div class="right">
                <div class="form-group">
                  <label for="author_name">Name of the Author  </label>
                  <input type="text" class="form-control" id="author_name" name="author_name" value="">
                </div>
                <div class="form-group">
                  <label for="author_designation">Designation  </label>
                  <input type="text" class="form-control" name="author_designation" id="author_designation" value="">
                </div>
                <button type="button" name="button">Submit</button>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>


  </div>
  <div id="delete" class="mfp-with-anim mfp-hide popup_modal_size">
    <div class="modal_popup">
      <i class="fas fa-trash-alt"></i>
      <h2>Are you sure you want to delete this section?</h2>
        <div class="actionbtn">
          <button type="button" name="button" class="mfp-close">Cancel</button>
          <button type="submit" name="button" id="delete_blog_btn" data-id="">Delete Section</button>
        </div>

    </div>
  </div>
  <script type="text/javascript" src="../js/jquery3.3.1.min.js"></script>
  <script type="text/javascript" src="../js/mdb.min.js"></script>
  <script type="text/javascript" src="../js/jquery.magnific-popup.min.js"></script>
  <script type="text/javascript">

  $('.popupmodal').magnificPopup({
    delegate: 'a.pop',
    removalDelay: 500,
    callbacks: {
      beforeOpen: function() {
        this.st.mainClass = this.st.el.attr('data-effect');
        // alert($("#delete_blog").attr("data"));
        // $("#delete_blog_btn").attr("data-id",$(".delete_blog").attr("data"));
      }
    },
    midClick: true
  });

  $(".delete_blog").on("click", function(){
    $("#delete_blog_btn").attr("data-id",$(this).attr("data"));
  })
  // Hinge effect popup
  $('a.hinge').magnificPopup({
    mainClass: 'mfp-with-fade',
    removalDelay: 1000, //delay removal by X to allow out-animation
    callbacks: {
      beforeClose: function() {
        this.content.addClass('hinge');
      },
      close: function() {
        this.content.removeClass('hinge');
      }
    },
    midClick: true
  });

  $("#delete_blog_btn").on("click", function(){
    var blog_id = $(this).attr("data-id");

    $.ajax({
      type: "POST",
      url: "delete.php",
      data: {"blog_id" : blog_id} ,
      // dataType: 'json',
      // contentType: "application/json; charset=utf-8",
      // cache: false,
      // processData: false,
      success: function(data)
      {
        alert("Deleted Successfully.");
        window.location.reload();
        // $("#form_alert").show();
        // setTimeout(function(){ $("#form_alert").css("display","block"); }, 8000);
        // location.reload();
        // alert("Form Submitted Successfully."); // show response from the php script.
      }
    });
  });
  </script>

</body>

</html>
