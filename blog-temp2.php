<?php include 'header.php';?>
<?php include 'blog_detail_view.php'; ?>

  <!-- Start your project here-->
  <div class="section23">
    <div class="container">
      <div class="row first_banner">
        <div class="col-sm-4">
          <h4 data-aos="fade-up"><?=$rowData['blog_heading']?></h4>
          <div class="social_sec" data-aos="fade-up">
            <ul>
              <li>Share</li>
              <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
              <li> <a href="#"><i class="fab fa-instagram"></i></a> </li>
              <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
              <li> <a href="#"><i class="fab fa-linkedin-in"></i></a> </li>
            </ul>
          </div>
        </div>
        <div class="col-sm-8">
          <img src="<?=str_replace("D:/projects/happydemic/","",$rowData['blog_thumbnail'])?>" alt="" data-aos="zoom-out">
        </div>
      </div>
      <div class="row second_banner">
        <div class="col-sm-4">
          <h5 data-aos="fade-up">Written By</h5>
          <div class="user_setail" data-aos="fade-up">
            <div class="user_img">
              <img src="<?=str_replace("D:/projects/happydemic/","",$rowData['author_image'])?>" alt="">
            </div>
            <div class="user_detail_text">
              <h4><?=$rowData['author_name']?></h4>
              <h5><?=$rowData['created_at']?></h5>
            </div>
          </div>
        </div>
        <div class="col-sm-8">
          <p data-aos="fade-up"><?=$rowData['blog_description']?></p>
          <div class="timing" data-aos="fade-up"><?=$rowData['timetoread']?> minutes to read <span><?=$rowData['created_at']?></span> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section24" data-aos="zoom-up" style="background-image:url(<?= str_replace("D:/projects/happydemic/","",$rowData['blog_banner'])?>)"></div>
  <div class="section25 container">
    <div class="row">
      <div class="col-sm-3">
        <h2 data-aos="fade-up">Sub heading</h2>
      </div>
      <div class="col-sm-7">
        <p data-aos="fade-up"><?=$rowData['first_content']?></p>
        <p data-aos="fade-up"><?=$rowData['second_content']?></p>
        <img src="<?=str_replace("D:/projects/happydemic/","",$rowData['middle_image'])?>" alt="">
        <p data-aos="fade-up"><?=$rowData['highlighted_content']?></p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <h2 data-aos="fade-up">Conclusion</h2>
      </div>
      <div class="col-sm-7">
        <p data-aos="fade-up"><?=$rowData['third_content']?></p>
        <div class="user_setail" data-aos="fade-up">
          <div class="user_img">
            <img src="<?=str_replace("D:/projects/happydemic/","",$rowData['author_image'])?>" alt="">
          </div>
          <div class="user_detail_text">
            <h4><?=$rowData['author_name']?></h4>
            <h5><?=$rowData['author_designation']?></h5>
          </div>
          <div class="social_sec" data-aos="fade-up">
            <ul>
              <li>Share</li>
              <li> <a href="#"><i class="fab fa-facebook-f"></i></a> </li>
              <li> <a href="#"><i class="fab fa-instagram"></i></a> </li>
              <li> <a href="#"><i class="fab fa-twitter"></i></a> </li>
              <li> <a href="#"><i class="fab fa-linkedin-in"></i></a> </li>
            </ul>
          </div>
        </div>
        <div class="tag_click" data-aos="fade-up">
          <ul>
            <?php foreach($blog_tags as $val): ?>
                <li> <a href="#"><?=$val?></a> </li>
              <!-- <li> <a href="#">Business</a> </li>
              <li> <a href="#">Engagement</a> </li> -->
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="section22">
    <div class="container">
    <?php if(!empty($relatedblogsList)):?>
      <div class="row">
        <div class="col">
          <h2 data-aos="fade-up">Related articles</h2>
          <div class="articles owl-carousel owl-theme">
            <?php foreach($relatedblogsList as $k=>$value): ?>
            <div class="item">
              <div class="card">
                <img src="<?=str_replace("D:/projects/happydemic/","",$value['blog_thumbnail'])?>" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title"><?=$value['blog_heading']?></h5>
                  <p class="card-text">
                    <?=$value['blog_description']?>
                  </p>
                </div>
              </div>
            </div>
            <?php endforeach; ?>
            
            <!-- <div class="item">
              <div class="card">
                <img src="img/blog/article.png" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title">Fostering high productivity
                    amongst employees</h5>
                  <p class="card-text">
                    A popular notion, music really does connect to everyone across the globe. Here ...
                  </p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="card">
                <img src="img/blog/article.png" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title">Fostering high productivity
                    amongst employees</h5>
                  <p class="card-text">
                    A popular notion, music really does connect to everyone across the globe. Here ...
                  </p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="card">
                <img src="img/blog/article.png" class="card-img" alt="..." />
                <div class="card-img-overlay">
                  <h5 class="card-title">Fostering high productivity
                    amongst employees</h5>
                  <p class="card-text">
                    A popular notion, music really does connect to everyone across the globe. Here ...
                  </p>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    <?php endif;?> 
    </div>
  </div>

  <div class="section9">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h2 data-aos="fade-up">Add Happydemic to my inbox</h2>
          <p data-aos="fade-up">We offer a distinctive voice to your orgainisation, relationships and talent.<br> Subscribe now!</p>
        </div>
        <div class="col-sm-6">
          <div class="form-outline" data-aos="fade-up">
            <i class="fas fa-envelope trailing"></i>
            <input type="text" id="form1" class="form-control form-icon-trailing" placeholder="Enter your email"/>
            <button class="btn btn-outline-primary" type="button" id="button-addon2" data-mdb-ripple-color="dark">
              Subscribe Here
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- End your project here-->
<?php include 'footer.php';?>
  <!-- MDB -->
  <script type="text/javascript" src="js/jquery3.3.1.min.js"></script>
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <!-- Custom scripts -->
  <script type="text/javascript">
    $(document).ready(function() {
      $('.articles').owlCarousel({
        stagePadding: 0,
        items: 3,
        loop: false,
        margin: 30,
        dots: false,
        autoplay: false,
        smartSpeed:1500,
        nav: true,
        navText: [
          "<img src='img/corporate/left_arrow.svg'>",
          "<img src='img/corporate/right_arrow.svg'>"
        ],
      });
      $(window).scroll(function() {
        var sticky = $('.header'),
          scroll = $(window).scrollTop();
        if (scroll >= 100) sticky.addClass('sticky');
        else sticky.removeClass('sticky');
      });
      $('.nav__content').click(function() {
        $('.menu').removeClass('active');
        $('body').removeClass('nav-active');
      });
    });
    const app = (() => {
      let body;
      let menu;

      const init = () => {
        body = document.querySelector('body');
        menu = document.querySelector('.menu');
        menuItems = document.querySelectorAll('.nav__list-item');
        applyListeners();
      }

      const applyListeners = () => {
        menu.addEventListener('click', () => toggleClass(body, 'nav-active'));
      }

      const toggleClass = (element, stringClass) => {
        if (element.classList.contains(stringClass))
          element.classList.remove(stringClass);
        else
          element.classList.add(stringClass);
      }

      init();
    })();

    document.querySelectorAll('.menu').forEach(btn => {
      btn.addEventListener('click', e => {
        btn.classList.toggle('active');
      });
    });

    var loader = $('.loader');
    var wHeight = $(window).height();
    var wWidth = $(window).width();
    var i = 0;
    loader.css({
      top: wHeight / 2 - 1.5,
      left: wWidth / 2 - 200
    })

    do {
      loader.animate({
        width: i
      }, 10)
      i += 2;
    } while (i <= 400)
    if (i === 402) {
      loader.animate({
        left: 0,
        width: '100%'
      })
      loader.animate({
        top: '0',
        height: '100vh'
      })
    }
  </script>
</body>

</html>
