<footer>
  <div class="container">
    <div class="row">
      <div class="col-6 col-lg-3 mobile_none">
        <a href="index.php"> <img src="img/home/logo_footer.svg" alt=""> </a>
        <ul class="list-unstyled">
          <li>
            <p>Corporate Office</p>
            <a href="https://www.google.com/maps/place/SS+House/@19.097338,72.8498999,15z/data=!4m5!3m4!1s0x0:0xb51e467e2bc09218!8m2!3d19.097338!4d72.8498999" target="_blank" class="sub_title">SS, House, Nehru Rd,<br> Vile Parle East, <br>Vile
              Parle, Mumbai, <br> Maharashtra  400057</a>
          </li>

        </ul>
      </div>
      <div class="col-6 col-lg-3">
        <h5 class="title_text">Profile</h5>
        <ul class="list-unstyled">
          <li>
            <a href="leadership.php" class="sub_title">Leadership</a>
          </li>
          <li>
            <a href="our-thinking.php" class="sub_title">Blogs and Vlogs</a>
          </li>
          <li>
            <a href="solutions.php" class="sub_title">Solutions</a>
          </li>
          <li>
            <a href="people.php" class="sub_title">People</a>
          </li>
          <li>
            <a href="careers.php" class="sub_title">Careers</a>
          </li>
          <li>
            <a href="news.php" class="sub_title">News</a>
          </li>
          <li>
            <a href="contact-us.php" class="sub_title">Contact us</a>
          </li>

        </ul>
      </div>
      <div class="col-6 col-lg-3">
        <h5 class="title_text">Segments</h5>
        <ul class="list-unstyled">
          <li>
            <a href="corporates.php" class="sub_title">Corporates</a>
          </li>
          <li>
            <a href="artists.php" class="sub_title">Artists</a>
          </li>
          <li>
            <a href="songstruck.php" class="sub_title">Individual</a>
          </li>
        </ul>
        <div class="desktop_none">
          <h5 class="title_text">Corporate Offerings</h5>
          <ul class="list-unstyled">
            <li>
              <a href="brand-essence.php" class="sub_title">Brand Essence</a>
            </li>
            <li>
              <a href="revenue-impact.php" class="sub_title">Revenue Impact</a>
            </li>
            <li>
              <a href="organisation-culture.php" class="sub_title">Organisation and Culture</a>
            </li>
            <li class="site_colaab mobile_none">
              <a href="http://www.colabindia.com/" class="sub_title" target="_blank">Site by Colab</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-6 col-lg-3 mobile_none">
        <h5 class="title_text">Corporate Offerings</h5>
        <ul class="list-unstyled">
          <li>
            <a href="brand-essence.php" class="sub_title">Brand Essence</a>
          </li>
          <li>
            <a href="revenue-impact.php" class="sub_title">Revenue Impact</a>
          </li>
          <li>
            <a href="organisation-culture.php" class="sub_title">Organisation and Culture</a>
          </li>
        <li class="social_icon">
          <a href="https://www.facebook.com/Happydemic" target="_blank"> <img src="img/fb.png" alt=""> </a>
          <a href="https://www.linkedin.com/company/happydemic/" target="_blank"> <img src="img/in.png" alt=""> </a>
          <a href="https://www.youtube.com/channel/UC4TGw3DFZ_yp45MsVJ-LCvQ" target="_blank"> <img src="img/youtube.png" alt=""> </a>
          <a href="https://twitter.com/happydemic_mike" target="_blank"> <img src="img/twitter.png" alt=""> </a>
            <a href="https://www.instagram.com/happydemic_mike/" target="_blank"> <img src="img/instagram.png" alt=""> </a>
          </li>
        </ul>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-3   desktop_none">
        <a href="index.php"> <img src="img/home/logo_footer.svg" alt=""> </a>
        <p>Corporate Office</p>

        <ul class="list-unstyled">
          <li>
            <a href="https://www.google.com/maps/place/SS+House/@19.097338,72.8498999,15z/data=!4m5!3m4!1s0x0:0xb51e467e2bc09218!8m2!3d19.097338!4d72.8498999" target="_blank" class="sub_title">SS, House, Nehru Rd,<br> Vile Parle East, <br>Vile
              Parle, Mumbai, <br> Maharashtra <br> 400057</a>
          </li>
          <li class="social_icon">
            <a href="https://www.facebook.com/Happydemic" target="_blank"> <img src="img/fb.png" alt=""> </a>
            <a href="https://www.linkedin.com/company/happydemic/" target="_blank"> <img src="img/in.png" alt=""> </a>
            <a href="https://www.youtube.com/channel/UC4TGw3DFZ_yp45MsVJ-LCvQ" target="_blank"> <img src="img/youtube.png" alt=""> </a>
            <a href="https://twitter.com/happydemic_mike" target="_blank"> <img src="img/twitter.png" alt=""> </a>
              <a href="https://www.instagram.com/happydemic_mike/" target="_blank"> <img src="img/instagram.png" alt=""> </a>
            </li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col colab_sec">
      <a href="http://www.colabindia.com/" class="sub_title" target="_blank">Site by Colab</a>
      </div>
    </div>
  </div>
</footer>

<script type="text/javascript" src="js/jquery3.3.1.min.js"></script>
<script type="text/javascript" src="js/mdb.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/owl.carousel2.thumbs.js"></script>
<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
<script src="js/aos.js"></script>
<script type="text/javascript">
// Inline popups
$('.openme').on('click', function() {
    $('body').addClass("nav-active");
});
$('.closeme').on('click', function() {
    $('body').removeClass("nav-active");
});
$('.menu').on('click', function() {
    $('.menu').toggleClass('active');
});

$('.popupmodal').magnificPopup({
  delegate: 'a.popme',
  removalDelay: 500,
  callbacks: {
    beforeOpen: function() {
      this.st.mainClass = this.st.el.attr('data-effect');
    }
  },
  midClick: true
});

// Hinge effect popup
$('a.hinge').magnificPopup({
  mainClass: 'mfp-with-fade',
  removalDelay: 1000, //delay removal by X to allow out-animation
  callbacks: {
    beforeClose: function() {
      this.content.addClass('hinge');
    },
    close: function() {
      this.content.removeClass('hinge');
    }
  },
  midClick: true
});
AOS.init({
  easing: 'ease-out-back',
  duration: 800,
  offset: 50,
  disable: false,
  easing: 'ease',
  once: true,
});
</script>
